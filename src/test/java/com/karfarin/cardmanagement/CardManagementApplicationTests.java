package com.karfarin.cardmanagement;

import com.karfarin.cardmanagement.domain.socket.service.ChannelManagerService;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import com.karfarin.cardmanagement.domain.tosan.model.channel.response.CardTransferResponse;
import com.karfarin.cardmanagement.enums.CardSwitchType;
import com.karfarin.cardmanagement.util.RouteingUtil;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import javax.xml.bind.*;
import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.net.Socket;
import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
class CardManagementApplicationTests {

  @Before
  public void setUp() {}

  @Test
  void contextLoads() {}

  @Test
  void testExecuteCommand() throws ServiceException, RemoteException {
    ChannelManagerService channelManagerService = new ChannelManagerService();
    String response =
        channelManagerService.executeCommand(
            "<request><cmd>CardTransfer</cmd><accountnumber>0100224466777</accountnumber><appname>RYErtebatFarda</appname><jobid>MBJ3075</jobid><branchid>996</branchid><userid>Test1234</userid><msgid>3122</msgid></request>CR/LF");
    assertThat(response).isEqualTo("");
  }

  @Test
  void testFindRoute() {
    String command = "TopupGetLastPackages-PAN,EBGetCustomerContact-PANSrc,MBFundTransfer-PAN";
    RouteingUtil.commands =
        Arrays.stream(command.split(","))
            .map(s -> s.split("-"))
            .collect(
                Collectors.toMap(
                    s -> s[0].toLowerCase(), s -> s.length > 1 ? s[1].toLowerCase() : ""));
    CardSwitchType switchType =
        RouteingUtil.findRoute(
            "<request><cmd>EBGetCustomerContact</cmd><appname>RYTeller</appname><msgid>5</msgid><jobid>5</jobid><BranchCode>000218</BranchCode><PANSrc>6274881112942056</PANSrc><STAN>5</STAN><DateAndTime>981102091114</DateAndTime><PSDCode>21010010314C</PSDCode><AcquiringTerminal>00165301</AcquiringTerminal><MobileNo>09124366192</MobileNo><OTPChannel>A</OTPChannel><OTPType>2</OTPType></request>");
    assertThat(switchType).isEqualTo(CardSwitchType.ICS);
  }

  @Test
  void testChannel() {
    Socket socket = null;
    try {
      socket = new Socket("10.5.16.151", 6000);
      socket.getOutputStream().write("salam".concat(System.lineSeparator()).getBytes("cp1256"));
      socket.getOutputStream().flush();
      BufferedReader bufferedReader =
          new BufferedReader(new InputStreamReader(socket.getInputStream(), "cp1256"));
      String response = bufferedReader.readLine();
      socket.close();

    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  @Test
  void testConvertObjectToXML() throws JAXBException {
    CardTransferResponse cr = new CardTransferResponse();
    cr.setMessageId("salam");
    cr.setCommand("test");
    cr.setReferenceNo(3216546);
    cr.setJobId("78");

    /*
        JAXBContext context = JAXBContext.newInstance(CardTransferResponse.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
        StringWriter stringWriter = new StringWriter();
        marshaller.marshal(cr, stringWriter);
        System.out.println(stringWriter.toString());
    */
    System.out.println(marshall(cr));
  }

  private <T> String marshall(T response) throws JAXBException {
    JAXBContext context = JAXBContext.newInstance(response.getClass());
    Marshaller marshaller = context.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
    StringWriter stringWriter = new StringWriter();
    marshaller.marshal(response, stringWriter);
    return stringWriter.toString();
  }
}
