package com.karfarin.cardmanagement.domain.tosan.model.channel.request;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.Transfer;
import lombok.Data;

import java.util.Date;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/22 11:30 AM
 */
@Data
public class CardTransferRequest extends Transfer {

  private Date settlementDate;
  private String mobileNumber;
  private String iP;
}
