package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum PinType {
  CARD,
  EPAY;
}
