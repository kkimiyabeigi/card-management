package com.karfarin.cardmanagement.domain.tosan.model.channel.base;

import lombok.Data;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co
 * @date 2020/08/15 09:46 AM
 */
@Data
public class ChannelRequest {
  private String command;
  private String applicationName;
  private String jobId;
  private String messageId;
  private String branchId;
  private String userId;
}
