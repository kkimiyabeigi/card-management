package com.karfarin.cardmanagement.domain.tosan.model.response;

import com.karfarin.cardmanagement.domain.tosan.model.CardBean;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@Data
public class CardResDTO {
  private List<CardBean> cardBeans;
}
