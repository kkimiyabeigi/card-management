package com.karfarin.cardmanagement.domain.tosan.model.channel.response;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;

@Data
public class CardTransferReverseResponse extends ChannelResponse {
  private String result;
  private Integer errorNo;
  private String description;
  private String farsiDescription;
  private String jobId;
  private String messageId;
}
