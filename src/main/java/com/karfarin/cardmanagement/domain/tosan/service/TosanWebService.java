package com.karfarin.cardmanagement.domain.tosan.service;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
public interface TosanWebService {

  /**
   * Calling Tosan web service
   *
   * @param request The channel request
   * @return The channel response
   */
  <T, G> T callTosanWebService(G request);
}
