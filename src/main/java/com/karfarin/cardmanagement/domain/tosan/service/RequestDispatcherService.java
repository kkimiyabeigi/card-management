package com.karfarin.cardmanagement.domain.tosan.service;

import com.karfarin.cardmanagement.domain.tosan.model.channel.request.CardTransferRequest;
import com.karfarin.cardmanagement.domain.tosan.model.channel.request.internet.*;
import com.karfarin.cardmanagement.domain.tosan.model.response.FinancialServiceResDTO;
import com.karfarin.cardmanagement.domain.tosan.service.internet.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.JAXBException;

@Service
public class RequestDispatcherService {

  private final Logger LOG = LoggerFactory.getLogger(getClass().getName());
  @Autowired private CardTransferReverseService cardTransferReverseService;
  @Autowired private EBCardAuthenticationService ebCardAuthenticationService;
  @Autowired private EBFundTransferService ebFundTransferService;
  @Autowired private EBTransferService ebTransferService;
  @Autowired private EBTransferValidateService ebTransferValidateService;
  @Autowired private GetCustomerCardsByClientIDService getCustomerCardsByClientIDService;

  /**
   * Dispatch request to correct service
   *
   * @param request The request object of channel
   * @param <T> The object of channel request
   * @return The String of xml
   */
  public <T> String sendRequest(T request) {
    try {
      if (request instanceof CardTransferRequest)
        return getCardTransferResponse((CardTransferRequest) request);
      if (request instanceof EBCardAuthenticationRequest)
        return getEBCardAuthenticationResponse((EBCardAuthenticationRequest) request);
      if (request instanceof EBFundTransferRequest)
        return getEBFundTransferResponse((EBFundTransferRequest) request);
      if (request instanceof EBTransferRequest)
        return getEBTransferResponse((EBTransferRequest) request);
      if (request instanceof EBTransferValidateRequest)
        return getEBTransferValidateResponse((EBTransferValidateRequest) request);
      if (request instanceof GetCustomerCardsByClientIDRequest)
        return getGetCustomerCardsByClientIDResponse((GetCustomerCardsByClientIDRequest) request);
    } catch (JAXBException e) {
      e.printStackTrace();
      LOG.error("There is an error for sendRequest method", e);
    }

    return "";
  }

  /**
   * Send request for card transfer
   *
   * @param request The request of card transfer
   * @return The string of xml
   * @throws JAXBException The exception of createChannelResponseObject
   */
  private String getCardTransferResponse(CardTransferRequest request) throws JAXBException {
    FinancialServiceResDTO response = cardTransferReverseService.callTosanWebService(request);
    ResponseFactoryService responseFactory = new ResponseFactoryService();
    return responseFactory.createChannelResponse(response, request);
  }

  private String getEBCardAuthenticationResponse(EBCardAuthenticationRequest request) throws JAXBException {
    FinancialServiceResDTO response = ebCardAuthenticationService.callTosanWebService(request);
    ResponseFactoryService responseFactory = new ResponseFactoryService();
    return responseFactory.createChannelResponse(response, request);
  }

  private String getEBFundTransferResponse(EBFundTransferRequest request) throws JAXBException {
    FinancialServiceResDTO response = ebFundTransferService.callTosanWebService(request);
    ResponseFactoryService responseFactory = new ResponseFactoryService();
    return responseFactory.createChannelResponse(response, request);
  }

  private String getEBTransferResponse(EBTransferRequest request) throws JAXBException {
    FinancialServiceResDTO response = ebTransferService.callTosanWebService(request);
    ResponseFactoryService responseFactory = new ResponseFactoryService();
    return responseFactory.createChannelResponse(response, request);
  }

  private String getEBTransferValidateResponse(EBTransferValidateRequest request) throws JAXBException {
    FinancialServiceResDTO response = ebTransferValidateService.callTosanWebService(request);
    ResponseFactoryService responseFactory = new ResponseFactoryService();
    return responseFactory.createChannelResponse(response, request);
  }

  private String getGetCustomerCardsByClientIDResponse(GetCustomerCardsByClientIDRequest request) throws JAXBException {
    FinancialServiceResDTO response = getCustomerCardsByClientIDService.callTosanWebService(request);
    ResponseFactoryService responseFactory = new ResponseFactoryService();
    return responseFactory.createChannelResponse(response, request);
  }
}
