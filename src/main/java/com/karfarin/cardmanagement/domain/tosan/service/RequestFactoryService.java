package com.karfarin.cardmanagement.domain.tosan.service;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelCommandType;
import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.request.CardTransferRequest;
import com.karfarin.cardmanagement.domain.tosan.model.channel.request.internet.*;
import com.karfarin.cardmanagement.domain.tosan.model.channel.request.mobile.*;
import com.karfarin.cardmanagement.util.RouteingUtil;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co
 * @date 2020/08/23 17:10 PM
 */
public class RequestFactoryService {

  private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
  private Element element;
  /**
   * Global converting channel request to object
   *
   * @param request The channel request
   * @param <T> The object of channel request
   * @return The object of request
   */
  @SuppressWarnings("unchecked")
  public <T> T createChannelRequestObject(String request) {
    String command =
        RouteingUtil.getValueByTagName(ChannelParameterType.COMMAND.toString(), request);
    if (StringUtils.isEmpty(command)) return null;
    else if (command.equalsIgnoreCase(ChannelCommandType.CARD_TRANSFER.toString()))
      return (T) createCardTransferRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.EB_CARD_AUTHENTICATION.toString()))
      return (T) createEBCardAuthenticationRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.EB_FUND_TRANSFER.toString()))
      return (T) createEBFundTransferRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.EB_TRANSFER.toString()))
      return (T) createEBTransferRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.EB_TRANSFER_VALIDATE.toString()))
      return (T) createEBTransferValidateRequest(request);
    else if (command.equalsIgnoreCase(
        ChannelCommandType.GET_CUSTOMER_CARDS_BY_CLIENT_ID.toString()))
      return (T) createGetCustomerCardsByClientIDRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.DELIVERED_NOTIFICATIONS.toString()))
      return (T) deliveredNotificationsRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.EB_GET_CUSTOMER_CONTACT.toString()))
      return (T) eBGetCustomerContactRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.GET_ACCOUNTS_BY_PAN.toString()))
      return (T) getAccountsByPANRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.MB_CARD_AUTHENTICATION.toString()))
      return (T) mBCardAuthenticationRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.MB_CARD_BLOCK.toString()))
      return (T) mBCardBlockRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.MB_FUND_TRANSFER.toString()))
      return (T) mBFundTransferRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.MB_GET_ACCOUNT_INFO.toString()))
      return (T) mBGetAccountInfoRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.MB_SET_CARD_PIN2.toString()))
      return (T) mBSetCardPIN2Request(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.MB_TRANSFER.toString()))
      return (T) mBTransferRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.MB_TRANSFER_VALIDATE.toString()))
      return (T) mBTransferValidateRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.OTP_GET_MOBILE_INFO.toString()))
      return (T) otpGetMobileInfoRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.OTP_MATCH_MOBILE_INFO.toString()))
      return (T) otpMatchMobileInfoRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.OTP_PROVISIONING.toString()))
      return (T) otpProvisioningRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.OTP_REQUEST_INTERNAL.toString()))
      return (T) otpRequestInternalRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.OTP_VERIFY_MOBILE_INFO.toString()))
      return (T) otpVerifyMobileInfoRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.PAY_BILL_BY_PAN.toString()))
      return (T) payBillByPANRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.SELL_PRODUCT_BY_PAN.toString()))
      return (T) sellProductByPANRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.TOP_UP_SELL_PRODUCT_BY_PAN.toString()))
      return (T) topUpSellProductByPANRequest(request);
    else if (command.equalsIgnoreCase(ChannelCommandType.VERIFY_V_CODE.toString()))
      return (T) verifyVCodeRequest(request);
    else if (command.equalsIgnoreCase(
        ChannelCommandType.GET_CUSTOMER_CARDS_BY_ACC_NO_REQUEST.toString()))
      return (T) getCustomerCardsByAccNoRequest(request);

    return null;
  }

  /**
   * Casting channel request to element
   *
   * @param request The channel request
   * @return The element
   * @throws ParserConfigurationException The exception of parsing
   * @throws IOException The exception of parsing
   * @throws SAXException The exception of parsing
   */
  private Element castChannelRequest(String request)
      throws ParserConfigurationException, IOException, SAXException {
    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
    ByteArrayInputStream input = new ByteArrayInputStream(request.getBytes(StandardCharsets.UTF_8));
    Document doc = dBuilder.parse(input);
    doc.getDocumentElement().normalize();
    NodeList nList = doc.getElementsByTagName(ChannelParameterType.REQUEST.toString());
    Node nNode = nList.item(0);
    return (Element) nNode;
  }

  /**
   * Creating CardTransferRequest
   *
   * @param request The channel request
   * @return CardTransferRequest
   */
  private CardTransferRequest createCardTransferRequest(String request) {
    try {
      CardTransferRequest result = new CardTransferRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setReferenceNo(
          Integer.valueOf(
              element
                  .getElementsByTagName(ChannelParameterType.REFERENCE_NO.toString())
                  .item(0)
                  .getTextContent()));
      result.setPanSource(
          element
              .getElementsByTagName(ChannelParameterType.PAN_SOURCE.toString())
              .item(0)
              .getTextContent());
      result.setPin2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCvv2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPanDestination(
          element
              .getElementsByTagName(ChannelParameterType.PAN_DESTINATION.toString())
              .item(0)
              .getTextContent());
      result.setSourceAccount(
          element
              .getElementsByTagName(ChannelParameterType.SOURCE_ACCOUNT.toString())
              .item(0)
              .getTextContent());
      result.setDestinationAccount(
          element
              .getElementsByTagName(ChannelParameterType.DESTINATION_ACCOUNT.toString())
              .item(0)
              .getTextContent());
      result.setAmount(
          element
              .getElementsByTagName(ChannelParameterType.AMOUNT.toString())
              .item(0)
              .getTextContent());
      result.setPsdCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setSettlementDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.SETTLEMENT_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setMobileNumber(
          element
              .getElementsByTagName(ChannelParameterType.MOBILE_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setIP(
          element
              .getElementsByTagName(ChannelParameterType.IP.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating DeliveredNotificationsRequest
   *
   * @param request The channel request
   * @return DeliveredNotificationsRequest
   */
  private DeliveredNotificationsRequest deliveredNotificationsRequest(String request) {
    try {
      DeliveredNotificationsRequest result = new DeliveredNotificationsRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating EBGetCustomerContactRequest
   *
   * @param request The channel request
   * @return EBGetCustomerContactRequest
   */
  private EBGetCustomerContactRequest eBGetCustomerContactRequest(String request) {
    try {
      EBGetCustomerContactRequest result = new EBGetCustomerContactRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setCustomerNumber(
          element
              .getElementsByTagName(ChannelParameterType.CUSTOMER_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setAccountType(
          element
              .getElementsByTagName(ChannelParameterType.ACCOUNT_TYPE.toString())
              .item(0)
              .getTextContent());
      result.setDevice(
          element
              .getElementsByTagName(ChannelParameterType.DEVICE.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating GetAccountsByPANRequest
   *
   * @param request The channel request
   * @return GetAccountsByPANRequest
   */
  private GetAccountsByPANRequest getAccountsByPANRequest(String request) {
    try {
      GetAccountsByPANRequest result = new GetAccountsByPANRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating MBCardAuthenticationRequest
   *
   * @param request The channel request
   * @return MBCardAuthenticationRequest
   */
  private MBCardAuthenticationRequest mBCardAuthenticationRequest(String request) {
    try {
      MBCardAuthenticationRequest result = new MBCardAuthenticationRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setCVV2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating MBCardBlockRequest
   *
   * @param request The channel request
   * @return MBCardBlockRequest
   */
  private MBCardBlockRequest mBCardBlockRequest(String request) {
    try {
      MBCardBlockRequest result = new MBCardBlockRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating MBFundTransferRequest
   *
   * @param request The channel request
   * @return MBFundTransferRequest
   */
  private MBFundTransferRequest mBFundTransferRequest(String request) {
    try {
      MBFundTransferRequest result = new MBFundTransferRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setAmount(
          element
              .getElementsByTagName(ChannelParameterType.AMOUNT.toString())
              .item(0)
              .getTextContent());
      result.setAccountNumber(
          element
              .getElementsByTagName(ChannelParameterType.ACCOUNT_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setAccountNumberDest(
          element
              .getElementsByTagName(ChannelParameterType.ACCOUNT_NUMBER_DESTINATION.toString())
              .item(0)
              .getTextContent());
      result.setRecInstID(
          element
              .getElementsByTagName(ChannelParameterType.RECORD_INSERT_ID.toString())
              .item(0)
              .getTextContent());
      result.setSettlementDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.SETTLEMENT_DATE.toString())
                  .item(0)
                  .getTextContent()));

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating MBGetAccountInfoRequest
   *
   * @param request The channel request
   * @return MBGetAccountInfoRequest
   */
  private MBGetAccountInfoRequest mBGetAccountInfoRequest(String request) {
    try {
      MBGetAccountInfoRequest result = new MBGetAccountInfoRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setAccountNumber(
          element
              .getElementsByTagName(ChannelParameterType.ACCOUNT_NUMBER.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating MBSetCardPIN2Request
   *
   * @param request The channel request
   * @return MBSetCardPIN2Request
   */
  private MBSetCardPIN2Request mBSetCardPIN2Request(String request) {
    try {
      MBSetCardPIN2Request result = new MBSetCardPIN2Request();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setAccountNumber(
          element
              .getElementsByTagName(ChannelParameterType.ACCOUNT_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setCVV2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setNewPIN2(
          element
              .getElementsByTagName(ChannelParameterType.NEW_PIN_2.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating MBTransferRequest
   *
   * @param request The channel request
   * @return MBTransferRequest
   */
  private MBTransferRequest mBTransferRequest(String request) {
    try {
      MBTransferRequest result = new MBTransferRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAccountNumber(
          element
              .getElementsByTagName(ChannelParameterType.ACCOUNT_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCVV2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setPANSource(
          element
              .getElementsByTagName(ChannelParameterType.PAN_SOURCE.toString())
              .item(0)
              .getTextContent());
      result.setPANDestination(
          element
              .getElementsByTagName(ChannelParameterType.PAN_DESTINATION.toString())
              .item(0)
              .getTextContent());
      result.setReferenceNo(
          element
              .getElementsByTagName(ChannelParameterType.REFERENCE_NO.toString())
              .item(0)
              .getTextContent());
      result.setAmount(
          element
              .getElementsByTagName(ChannelParameterType.AMOUNT.toString())
              .item(0)
              .getTextContent());
      result.setSourceAccount(
          element
              .getElementsByTagName(ChannelParameterType.SOURCE_ACCOUNT.toString())
              .item(0)
              .getTextContent());
      result.setDestinationAccount(
          element
              .getElementsByTagName(ChannelParameterType.DESTINATION_ACCOUNT.toString())
              .item(0)
              .getTextContent());
      result.setSettlementDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.SETTLEMENT_DATE.toString())
                  .item(0)
                  .getTextContent()));

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating MBTransferValidateRequest
   *
   * @param request The channel request
   * @return MBTransferValidateRequest
   */
  private MBTransferValidateRequest mBTransferValidateRequest(String request) {
    try {
      MBTransferValidateRequest result = new MBTransferValidateRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCVV2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setPANSource(
          element
              .getElementsByTagName(ChannelParameterType.PAN_SOURCE.toString())
              .item(0)
              .getTextContent());
      result.setPANDestination(
          element
              .getElementsByTagName(ChannelParameterType.PAN_DESTINATION.toString())
              .item(0)
              .getTextContent());
      result.setReferenceNo(
          element
              .getElementsByTagName(ChannelParameterType.REFERENCE_NO.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating OtpGetMobileInfoRequest
   *
   * @param request The channel request
   * @return OtpGetMobileInfoRequest
   */
  private OtpGetMobileInfoRequest otpGetMobileInfoRequest(String request) {
    try {
      OtpGetMobileInfoRequest result = new OtpGetMobileInfoRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setMobileNo(
          element
              .getElementsByTagName(ChannelParameterType.MOBILE_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setDevice(
          element
              .getElementsByTagName(ChannelParameterType.DEVICE.toString())
              .item(0)
              .getTextContent());
      result.setNationalId(
          element
              .getElementsByTagName(ChannelParameterType.NATIONAL_ID.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating OtpMatchMobileInfoRequest
   *
   * @param request The channel request
   * @return OtpMatchMobileInfoRequest
   */
  private OtpMatchMobileInfoRequest otpMatchMobileInfoRequest(String request) {
    try {
      OtpMatchMobileInfoRequest result = new OtpMatchMobileInfoRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setMobileNo(
          element
              .getElementsByTagName(ChannelParameterType.MOBILE_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setDevice(
          element
              .getElementsByTagName(ChannelParameterType.DEVICE.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating OtpProvisioningRequest
   *
   * @param request The channel request
   * @return OtpProvisioningRequest
   */
  private OtpProvisioningRequest otpProvisioningRequest(String request) {
    try {
      OtpProvisioningRequest result = new OtpProvisioningRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setMobileNo(
          element
              .getElementsByTagName(ChannelParameterType.MOBILE_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setSTAN(
          element
              .getElementsByTagName(ChannelParameterType.STAN.toString())
              .item(0)
              .getTextContent());
      result.setDateAndTime(
          element
              .getElementsByTagName(ChannelParameterType.DATE_AND_TIME.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setOTPChannel(
          element
              .getElementsByTagName(ChannelParameterType.OTP_CHANNEL.toString())
              .item(0)
              .getTextContent());
      result.setOTPType(
          element
              .getElementsByTagName(ChannelParameterType.OTP_TYPE.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating OtpRequestInternalRequest
   *
   * @param request The channel request
   * @return OtpRequestInternalRequest
   */
  private OtpRequestInternalRequest otpRequestInternalRequest(String request) {
    try {
      OtpRequestInternalRequest result = new OtpRequestInternalRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setSTAN(
          element
              .getElementsByTagName(ChannelParameterType.STAN.toString())
              .item(0)
              .getTextContent());
      result.setDateAndTime(
          element
              .getElementsByTagName(ChannelParameterType.DATE_AND_TIME.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setCharge(
          element
              .getElementsByTagName(ChannelParameterType.CHARGE.toString())
              .item(0)
              .getTextContent());
      result.setCharge(
          element
              .getElementsByTagName(ChannelParameterType.CHARGE.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setDevice(
          element
              .getElementsByTagName(ChannelParameterType.DEVICE.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating OtpVerifyMobileInfoRequest
   *
   * @param request The channel request
   * @return OtpVerifyMobileInfoRequest
   */
  private OtpVerifyMobileInfoRequest otpVerifyMobileInfoRequest(String request) {
    try {
      OtpVerifyMobileInfoRequest result = new OtpVerifyMobileInfoRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setDevice(
          element
              .getElementsByTagName(ChannelParameterType.DEVICE.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setMobileNo(
          element
              .getElementsByTagName(ChannelParameterType.MOBILE_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setNationalId(
          element
              .getElementsByTagName(ChannelParameterType.NATIONAL_ID.toString())
              .item(0)
              .getTextContent());
      result.setVerificationCode(
          element
              .getElementsByTagName(ChannelParameterType.VERIFICATION_CODE.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating PayBillByPANRequest
   *
   * @param request The channel request
   * @return PayBillByPANRequest
   */
  private PayBillByPANRequest payBillByPANRequest(String request) {
    try {
      PayBillByPANRequest result = new PayBillByPANRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCVV2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setBillcode(
          element
              .getElementsByTagName(ChannelParameterType.BILL_CODE.toString())
              .item(0)
              .getTextContent());
      result.setChannel(
          element
              .getElementsByTagName(ChannelParameterType.CHANNEL.toString())
              .item(0)
              .getTextContent());
      result.setSettlementDate(
          element
              .getElementsByTagName(ChannelParameterType.SETTLEMENT_DATE.toString())
              .item(0)
              .getTextContent());
      result.setSTANCard(
          element
              .getElementsByTagName(ChannelParameterType.STAN_CARD.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setUserApp(
          element
              .getElementsByTagName(ChannelParameterType.USER_APP.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating SellProductByPANRequest
   *
   * @param request The channel request
   * @return SellProductByPANRequest
   */
  private SellProductByPANRequest sellProductByPANRequest(String request) {
    try {
      SellProductByPANRequest result = new SellProductByPANRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setSettlementDate(
          element
              .getElementsByTagName(ChannelParameterType.SETTLEMENT_DATE.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCVV2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setMobileNo(
          element
              .getElementsByTagName(ChannelParameterType.MOBILE_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setInitiatorDateTime(
          element
              .getElementsByTagName(ChannelParameterType.INITIATOR_DATE_TIME.toString())
              .item(0)
              .getTextContent());
      result.setSTAN(
          element
              .getElementsByTagName(ChannelParameterType.STAN.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTellerIdTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TELLER_ID_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setDeviceCode(
          element
              .getElementsByTagName(ChannelParameterType.DEVICE_CODE.toString())
              .item(0)
              .getTextContent());
      result.setCompanyCode(
          element
              .getElementsByTagName(ChannelParameterType.COMPANY_CODE.toString())
              .item(0)
              .getTextContent());
      result.setPlanCode(
          element
              .getElementsByTagName(ChannelParameterType.PLAN_CODE.toString())
              .item(0)
              .getTextContent());
      result.setProductCode(
          element
              .getElementsByTagName(ChannelParameterType.PRODUCT_CODE.toString())
              .item(0)
              .getTextContent());
      result.setUserApp(
          element
              .getElementsByTagName(ChannelParameterType.USER_APP.toString())
              .item(0)
              .getTextContent());
      result.setInfof41(
          element
              .getElementsByTagName(ChannelParameterType.INF_OF_41.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setAmount(
          element
              .getElementsByTagName(ChannelParameterType.AMOUNT.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating TopUpSellProductByPANRequest
   *
   * @param request The channel request
   * @return TopUpSellProductByPANRequest
   */
  private TopUpSellProductByPANRequest topUpSellProductByPANRequest(String request) {
    try {
      TopUpSellProductByPANRequest result = new TopUpSellProductByPANRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setSettlementDate(
          element
              .getElementsByTagName(ChannelParameterType.SETTLEMENT_DATE.toString())
              .item(0)
              .getTextContent());
      result.setPAN(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setPIN2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCVV2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setMobileNo(
          element
              .getElementsByTagName(ChannelParameterType.MOBILE_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setInitiatorDateTime(
          element
              .getElementsByTagName(ChannelParameterType.INITIATOR_DATE_TIME.toString())
              .item(0)
              .getTextContent());
      result.setSTAN(
          element
              .getElementsByTagName(ChannelParameterType.STAN.toString())
              .item(0)
              .getTextContent());
      result.setDeviceCode(
          element
              .getElementsByTagName(ChannelParameterType.DEVICE_CODE.toString())
              .item(0)
              .getTextContent());
      result.setCompanyCode(
          element
              .getElementsByTagName(ChannelParameterType.COMPANY_CODE.toString())
              .item(0)
              .getTextContent());
      result.setPlanCode(
          element
              .getElementsByTagName(ChannelParameterType.PLAN_CODE.toString())
              .item(0)
              .getTextContent());
      result.setProductCode(
          element
              .getElementsByTagName(ChannelParameterType.PRODUCT_CODE.toString())
              .item(0)
              .getTextContent());
      result.setUserApp(
          element
              .getElementsByTagName(ChannelParameterType.USER_APP.toString())
              .item(0)
              .getTextContent());
      result.setInfof41(
          element
              .getElementsByTagName(ChannelParameterType.INF_OF_41.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setAmount(
          element
              .getElementsByTagName(ChannelParameterType.AMOUNT.toString())
              .item(0)
              .getTextContent());
      result.setTellerId(
          element
              .getElementsByTagName(ChannelParameterType.TELLER_ID.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating VerifyVCodeRequest
   *
   * @param request The channel request
   * @return VerifyVCodeRequest
   */
  private VerifyVCodeRequest verifyVCodeRequest(String request) {
    try {
      VerifyVCodeRequest result = new VerifyVCodeRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setMobileNo(
          element
              .getElementsByTagName(ChannelParameterType.MOBILE_NUMBER.toString())
              .item(0)
              .getTextContent());
      result.setClientNo(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());
      result.setDevice(
          element
              .getElementsByTagName(ChannelParameterType.DEVICE.toString())
              .item(0)
              .getTextContent());
      result.setTimestamp(
          element
              .getElementsByTagName(ChannelParameterType.TIMESTAMP.toString())
              .item(0)
              .getTextContent());
      result.setVerificationCode(
          element
              .getElementsByTagName(ChannelParameterType.VERIFICATION_CODE.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating GetCustomerCardsByAccNoRequest
   *
   * @param request The channel request
   * @return GetCustomerCardsByAccNoRequest
   */
  private GetCustomerCardsByAccNoRequest getCustomerCardsByAccNoRequest(String request) {
    try {
      GetCustomerCardsByAccNoRequest result = new GetCustomerCardsByAccNoRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating EBCardAuthenticationRequest
   *
   * @param request The channel request
   * @return EBCardAuthenticationRequest
   */
  private EBCardAuthenticationRequest createEBCardAuthenticationRequest(String request) {
    try {
      EBCardAuthenticationRequest result = new EBCardAuthenticationRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setPan(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setPin2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCvv2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPsdCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating EBFundTransferRequest
   *
   * @param request The channel request
   * @return EBFundTransferRequest
   */
  private EBFundTransferRequest createEBFundTransferRequest(String request) {
    try {
      EBFundTransferRequest result = new EBFundTransferRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setAmount(
          element
              .getElementsByTagName(ChannelParameterType.AMOUNT.toString())
              .item(0)
              .getTextContent());
      result.setAccountNumber(
          element
              .getElementsByTagName(ChannelParameterType.ACCOUNT_NUMBER2.toString())
              .item(0)
              .getTextContent());
      result.setAccountNumberDest(
          element
              .getElementsByTagName(ChannelParameterType.ACCOUNT_NUMBER_DEST.toString())
              .item(0)
              .getTextContent());
      result.setDescription(
          element
              .getElementsByTagName(ChannelParameterType.DESCRIPTION.toString())
              .item(0)
              .getTextContent());
      result.setRecInstID(
          element
              .getElementsByTagName(ChannelParameterType.REC_INST_ID.toString())
              .item(0)
              .getTextContent());
      result.setSettlementDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.SETTLEMENT_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPan(
          element
              .getElementsByTagName(ChannelParameterType.PAN.toString())
              .item(0)
              .getTextContent());
      result.setPaymentId(
          element
              .getElementsByTagName(ChannelParameterType.PAYMENT_ID.toString())
              .item(0)
              .getTextContent());
      result.setCrPaymentId(
          element
              .getElementsByTagName(ChannelParameterType.CR_PAYMENT_ID.toString())
              .item(0)
              .getTextContent());
      result.setDestinationDescription(
          element
              .getElementsByTagName(ChannelParameterType.DESTINATION_DESC.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating EBTransferRequest
   *
   * @param request The channel request
   * @return EBTransferRequest
   */
  private EBTransferRequest createEBTransferRequest(String request) {
    try {
      EBTransferRequest result = new EBTransferRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setReferenceNumber(
          element
              .getElementsByTagName(ChannelParameterType.REFERENCE_NO.toString())
              .item(0)
              .getTextContent());
      result.setPanSource(
          element
              .getElementsByTagName(ChannelParameterType.PAN_SOURCE.toString())
              .item(0)
              .getTextContent());
      result.setPin2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCvv2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPanDestination(
          element
              .getElementsByTagName(ChannelParameterType.PAN_DESTINATION.toString())
              .item(0)
              .getTextContent());
      result.setSourceAccount(
          element
              .getElementsByTagName(ChannelParameterType.SOURCE_ACCOUNT.toString())
              .item(0)
              .getTextContent());
      result.setDestinationAccount(
          element
              .getElementsByTagName(ChannelParameterType.DESTINATION_ACCOUNT.toString())
              .item(0)
              .getTextContent());
      result.setAmount(
          element
              .getElementsByTagName(ChannelParameterType.AMOUNT.toString())
              .item(0)
              .getTextContent());
      result.setPsdCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating EBTransferValidateRequest
   *
   * @param request The channel request
   * @return EBTransferValidateRequest
   */
  private EBTransferValidateRequest createEBTransferValidateRequest(String request) {
    try {
      EBTransferValidateRequest result = new EBTransferValidateRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setPanSource(
          element
              .getElementsByTagName(ChannelParameterType.PAN_SOURCE.toString())
              .item(0)
              .getTextContent());
      result.setPin2(
          element
              .getElementsByTagName(ChannelParameterType.PIN_2.toString())
              .item(0)
              .getTextContent());
      result.setCvv2(
          element
              .getElementsByTagName(ChannelParameterType.CVV_2.toString())
              .item(0)
              .getTextContent());
      result.setExpireDate(
          dateFormat.parse(
              element
                  .getElementsByTagName(ChannelParameterType.EXPIRE_DATE.toString())
                  .item(0)
                  .getTextContent()));
      result.setPanDestination(
          element
              .getElementsByTagName(ChannelParameterType.PAN_DESTINATION.toString())
              .item(0)
              .getTextContent());
      result.setPsdCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setReferenceNumber(
          element
              .getElementsByTagName(ChannelParameterType.REFERENCE_NO.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Creating GetCustomerCardsByClientIDRequest
   *
   * @param request The channel request
   * @return GetCustomerCardsByClientIDRequest
   */
  private GetCustomerCardsByClientIDRequest createGetCustomerCardsByClientIDRequest(
      String request) {
    try {
      GetCustomerCardsByClientIDRequest result = new GetCustomerCardsByClientIDRequest();
      element = castChannelRequest(request);
      result.setCommand(
          element
              .getElementsByTagName(ChannelParameterType.COMMAND.toString())
              .item(0)
              .getTextContent());
      result.setApplicationName(
          element
              .getElementsByTagName(ChannelParameterType.APPLICATION_NAME.toString())
              .item(0)
              .getTextContent());
      result.setJobId(
          element
              .getElementsByTagName(ChannelParameterType.JOB_ID.toString())
              .item(0)
              .getTextContent());
      result.setMessageId(
          element
              .getElementsByTagName(ChannelParameterType.MESSAGE_ID.toString())
              .item(0)
              .getTextContent());
      result.setBranchId(
          element
              .getElementsByTagName(ChannelParameterType.BRANCH_ID.toString())
              .item(0)
              .getTextContent());
      result.setUserId(
          element
              .getElementsByTagName(ChannelParameterType.USER_ID.toString())
              .item(0)
              .getTextContent());
      result.setAcquiringTerminal(
          element
              .getElementsByTagName(ChannelParameterType.ACQUIRING_TERMINAL.toString())
              .item(0)
              .getTextContent());
      result.setPSDCode(
          element
              .getElementsByTagName(ChannelParameterType.PSD_CODE.toString())
              .item(0)
              .getTextContent());
      result.setClientNumber(
          element
              .getElementsByTagName(ChannelParameterType.CLIENT_NO.toString())
              .item(0)
              .getTextContent());

      return result;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return null;
  }
}
