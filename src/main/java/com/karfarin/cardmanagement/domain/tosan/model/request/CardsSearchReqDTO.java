package com.karfarin.cardmanagement.domain.tosan.model.request;

import com.karfarin.cardmanagement.domain.tosan.model.enums.CardStatusType;
import lombok.Data;

@Data
public class CardsSearchReqDTO {
  private CardStatusType cardStatusType;
  private String cif;
  private String depositNumber;
  private Long length;
  private Long offset;
  private String pan;
}
