package com.karfarin.cardmanagement.domain.tosan.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Amount {
  private String currency;
  private BigDecimal value;
}
