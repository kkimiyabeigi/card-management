package com.karfarin.cardmanagement.domain.tosan.model.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Data
public class IssuerCustomReturnResDTO {
  private String trackingNumber;
  private FinancialServiceResDTO financialServiceRes;
}
