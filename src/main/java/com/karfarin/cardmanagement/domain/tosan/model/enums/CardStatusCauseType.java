package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum CardStatusCauseType {
  OK,
  STOLEN_CARD,
  LOST_CARD,
  ARBITRATION_FIAT,
  REPLICATED_CARD,
  EXPIRED_CARD,
  PIN_TRYIES_EXCCEDED,
  REPLICATED_CARD_CAPTURED,
  OTHER
}
