package com.karfarin.cardmanagement.domain.tosan.model.request;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ChangePasswordReqDTO {
  @NotNull private String currentPassword;
  @NotNull private String newPassword;
}
