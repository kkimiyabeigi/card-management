package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum ProcessType {
  PURCHASE,
  CASH,
  PURCHASE_REVERSE
}
