package com.karfarin.cardmanagement.domain.tosan.model.response;

import com.karfarin.cardmanagement.domain.tosan.model.RelatedDepositOfCard;
import lombok.Data;
import lombok.NonNull;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@Data
public class CardDepositResDTO {
  private List<RelatedDepositOfCard> deposits;
  @NonNull private Long totalRecord;
}
