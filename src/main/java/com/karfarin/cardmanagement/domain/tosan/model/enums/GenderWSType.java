package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum GenderWSType {
  MALE,
  FEMALE,
  LEGAL,
  PUBLIC;
}
