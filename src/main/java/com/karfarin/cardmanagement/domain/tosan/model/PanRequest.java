package com.karfarin.cardmanagement.domain.tosan.model;

import lombok.Data;

@Data
public class PanRequest {
  private String cif;
  private String pan;
}
