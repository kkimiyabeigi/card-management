package com.karfarin.cardmanagement.domain.tosan.model.response;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Data
public class CardOwnerResDTO {
  private String firstName;
  private String lastName;
  private String twoPhaseReferenceCode;
}
