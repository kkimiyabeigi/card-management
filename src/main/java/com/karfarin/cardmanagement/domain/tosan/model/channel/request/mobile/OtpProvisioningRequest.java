package com.karfarin.cardmanagement.domain.tosan.model.channel.request.mobile;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;

/**
 * @authors Fereydun Rashidabadi <f.rashidabadi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Data
public class OtpProvisioningRequest extends ChannelRequest {

  private String mobileNo;
  private String PAN;
  private String STAN;
  private String dateAndTime;
  private String PSDCode;
  private String OTPChannel;
  private String OTPType;
}
