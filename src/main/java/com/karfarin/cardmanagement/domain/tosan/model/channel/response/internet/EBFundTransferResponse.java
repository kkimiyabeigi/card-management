package com.karfarin.cardmanagement.domain.tosan.model.channel.response.internet;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = ChannelParameterType.Name.RESPONSE)
@XmlAccessorType(XmlAccessType.FIELD)
public class EBFundTransferResponse extends ChannelResponse {

    @XmlElement(name = ChannelParameterType.Name.HOST_DATE_AND_TIME)
    private Date hostDateTime;

    @XmlElement(name = ChannelParameterType.Name.STAN_RESPONSE)
    private String stan;

    @XmlElement(name = ChannelParameterType.Name.PAN_RESPONSE)
    private String pan;

    @XmlElement(name = ChannelParameterType.Name.DATE_AND_TIME_RESPONSE)
    private Date dateAndTime;
}
