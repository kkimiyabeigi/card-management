package com.karfarin.cardmanagement.domain.tosan.model.response;

import com.karfarin.cardmanagement.domain.tosan.model.CardDeposit;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Data
public class FinancialServiceResDTO {
  private CardDeposit deposit;
  private String switchResponseRRN;
}
