package com.karfarin.cardmanagement.domain.tosan.service;

import com.karfarin.cardmanagement.domain.tosan.model.channel.request.CardTransferRequest;
import com.karfarin.cardmanagement.domain.tosan.model.enums.DepositOrPanWSType;
import com.karfarin.cardmanagement.domain.tosan.model.enums.PinType;
import com.karfarin.cardmanagement.domain.tosan.model.request.FundTransferReqDTO;
import com.karfarin.cardmanagement.domain.tosan.model.response.FinancialServiceResDTO;
import com.karfarin.cardmanagement.util.ApiUtil;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CardTransferReverseService implements TosanWebService {

  @Override
  @SuppressWarnings("unchecked")
  public <T, G> T callTosanWebService(G request) {
    CardTransferRequest cardTransferRequest = (CardTransferRequest) request;
    FundTransferReqDTO tosanRequest = createFundTransferReqDTO(cardTransferRequest);
    FinancialServiceResDTO tosanResponse = new FinancialServiceResDTO();

    return (T)
        ApiUtil.sendRestApi(
            tosanResponse, tosanRequest, HttpMethod.POST, "{context name}/card/transfer");
  }

  /**
   * Generate FundTransferReqDTO
   *
   * @param request The CardTransferRequest
   * @return The FundTransferReqDTO
   */
  private FundTransferReqDTO createFundTransferReqDTO(CardTransferRequest request) {
    FundTransferReqDTO result = new FundTransferReqDTO();
    result.setAmount(new BigDecimal(request.getAmount()));
    result.setCvv2(request.getCvv2());
    result.setDestination(request.getPanDestination());
    result.setDestinationType(DepositOrPanWSType.PAN);
    result.setExpDate(String.valueOf(request.getExpireDate()));
    result.setMerchantId("");
    result.setPan("");
    result.setPin(request.getPin2());
    result.setPinType(PinType.CARD);
    result.setPspCode("");
    result.setTrack2("");
    result.setTwoPhaseReferenceCode("");

    return result;
  }
}
