package com.karfarin.cardmanagement.domain.tosan.model;

import lombok.Data;

@Data
public class SubscriberUserInfo {
  String id;
  String name;
}
