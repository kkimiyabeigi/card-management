package com.karfarin.cardmanagement.domain.tosan.model.request;

import com.karfarin.cardmanagement.domain.tosan.model.CardAuthorizeParams;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CardBalanceReqDTO {
  @NotNull private CardAuthorizeParams cardAuthorizeParams;
  private String depositNumber;
  private String merchantId;
  @NotNull private String pan;
}
