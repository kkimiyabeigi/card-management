package com.karfarin.cardmanagement.domain.tosan.model.response;

import com.karfarin.cardmanagement.domain.tosan.model.enums.CardStateType;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Data
public class CardSummaryResDTO {
  private String cardPan;
  private CardStateType cardState;
  private String cif;
  private String depositNumber;
  private String iban;
}
