package com.karfarin.cardmanagement.domain.tosan.model.response;

import lombok.Data;
import lombok.NonNull;
import lombok.ToString;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@Data
@ToString(exclude = {"secretKey"})
public class ActivateCardOtpTokenResDTO {
  @NonNull private Integer firstOtpLength;
  @NonNull private Integer otpGenerationPeriodInSeconds;
  @NonNull private Integer secondOtpLength;
  private String secretKey;
}
