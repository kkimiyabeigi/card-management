package com.karfarin.cardmanagement.domain.tosan.model.channel.request.mobile;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;

import java.util.Date;

/**
 * @authors Fereydun Rashidabadi <f.rashidabadi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Data
public class MBTransferRequest extends ChannelRequest {

  private String accountNumber;
  private String PIN2;
  private String CVV2;
  private Date expireDate;
  private String PSDCode;
  private String clientNo;
  private String PANSource;
  private String PANDestination;
  private String referenceNo;
  private String amount;
  private String sourceAccount;
  private String destinationAccount;
  private Date settlementDate;
}
