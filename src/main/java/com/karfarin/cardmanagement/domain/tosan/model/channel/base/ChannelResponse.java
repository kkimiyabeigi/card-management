package com.karfarin.cardmanagement.domain.tosan.model.channel.base;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/22 09:46 AM
 */
@Data
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class ChannelResponse {
  @XmlElement(name = ChannelParameterType.Name.COMMAND)
  private String command;

  @XmlElement(name = ChannelParameterType.Name.RESULT)
  private String result;

  @XmlElement(name = ChannelParameterType.Name.ERROR_NO)
  private Integer errorNo;

  @XmlElement(name = ChannelParameterType.Name.DESCRIPTION)
  private String description;

  @XmlElement(name = ChannelParameterType.Name.FARSI_DESCRIPTION)
  private String farsiDescription;

  @XmlElement(name = ChannelParameterType.Name.JOB_ID)
  private String jobId;

  @XmlElement(name = ChannelParameterType.Name.MESSAGE_ID)
  private String messageId;
}
