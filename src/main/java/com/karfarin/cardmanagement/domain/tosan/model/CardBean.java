package com.karfarin.cardmanagement.domain.tosan.model;

import com.karfarin.cardmanagement.domain.tosan.model.enums.CardResponseType;
import com.karfarin.cardmanagement.domain.tosan.model.enums.CardStatusCauseType;
import com.karfarin.cardmanagement.domain.tosan.model.enums.CardStatusType;
import com.karfarin.cardmanagement.domain.tosan.model.enums.CardType;

import java.util.Date;

public class CardBean {
  private CardStatusType cardStatus;
  private CardStatusCauseType cardStatusCause;
  private CardType cardType;
  private CardResponseType cardResponseType;
  private String customerFirstName;
  private String customerLastName;
  private String depositNumber;
  private Date expireDate;
  private Date issueDate;
  private String pan;
}
