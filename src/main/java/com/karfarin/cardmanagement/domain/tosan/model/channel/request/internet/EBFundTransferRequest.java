package com.karfarin.cardmanagement.domain.tosan.model.channel.request.internet;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;

import java.util.Date;

@Data
public class EBFundTransferRequest extends ChannelRequest {

  private String acquiringTerminal;
  private String amount;
  private String accountNumber;
  private String accountNumberDest;
  private String description;
  private String recInstID;
  private Date settlementDate;
  private String pan;
  private String paymentId;
  private String crPaymentId;
  private String destinationDescription;
}
