package com.karfarin.cardmanagement.domain.tosan.model.channel.response.internet;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = ChannelParameterType.Name.RESPONSE)
@XmlAccessorType(XmlAccessType.FIELD)
public class EBTransferResponse extends ChannelResponse {

    @XmlElement(name = ChannelParameterType.Name.REFERENCE_NO_RESPONSE)
    private String referenceNumber;

    @XmlElement(name = ChannelParameterType.Name.DATE_AND_TIME_RESPONSE)
    private Date dateAndTime;

    @XmlElement(name = ChannelParameterType.Name.ORIGIN_ACQUIRER_INSTITUTION_ID_RESPONSE)
    private String originAcquirerInstitutionId;

    @XmlElement(name = ChannelParameterType.Name.ADDITIONAL_PRIVATE_DATA)
    private String additionalPrivateData;
}