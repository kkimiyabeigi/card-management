package com.karfarin.cardmanagement.domain.tosan.service;

import com.karfarin.cardmanagement.domain.tosan.model.request.CardOwnerReqDTO;
import com.karfarin.cardmanagement.domain.tosan.model.response.CardOwnerResDTO;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class ECardAuthenticationService implements TosanWebService {

  private CardOwnerReqDTO cardOwnerReqDTO;
  private CardOwnerResDTO cardOwnerResDTO;
  private HttpMethod post = HttpMethod.POST;
  private String uri = "{context name}/card/owner";

  @Override
  public <T, G> T callTosanWebService(G request) {

    return null;
  }
}
