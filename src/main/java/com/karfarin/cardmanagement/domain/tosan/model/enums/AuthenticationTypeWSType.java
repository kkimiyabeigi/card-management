package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum AuthenticationTypeWSType {
  STATIC_USERNAME_CHANNEL,
  OTP1,
  OTP2,
  OTP3;
}
