package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum CardDepositType {
  MAIN,
  SECONDARY;
}
