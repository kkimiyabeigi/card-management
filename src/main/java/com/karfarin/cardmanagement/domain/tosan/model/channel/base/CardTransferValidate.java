package com.karfarin.cardmanagement.domain.tosan.model.channel.base;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * @authors Fereydun Rashidabadi <f.rashidabadi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CardTransferValidate extends ChannelRequest {

  private String panSource;
  private String pin2;
  private String cvv2;
  private Date expireDate;
  private String panDestination;
  private String psdCode;
  private String acquiringTerminal;
  private Integer referenceNo;
}
