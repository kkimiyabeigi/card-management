package com.karfarin.cardmanagement.domain.tosan.model.channel.request.mobile;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;

import java.util.Date;

/**
 * @authors Fereydun Rashidabadi <f.rashidabadi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Data
public class PayBillByPANRequest extends ChannelRequest {

  private String acquiringTerminal;
  private String PIN2;
  private String CVV2;
  private Date expireDate;
  private String PAN;
  private String billcode;
  private String channel;
  private String settlementDate;
  private String STANCard;
  private String clientNo;
  private String PSDCode;
  private String userApp;
}
