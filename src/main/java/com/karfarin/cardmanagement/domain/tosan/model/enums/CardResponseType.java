package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum CardResponseType {
  DEBIT,
  CREDIT,
  GIFT,
  CORPORATION,
  LOAN,
  DEPOSIT
}
