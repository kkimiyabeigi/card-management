package com.karfarin.cardmanagement.domain.tosan.model.channel.response;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/22 11:30 AM
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = ChannelParameterType.Name.RESPONSE)
@XmlAccessorType(XmlAccessType.FIELD)
public class CardTransferResponse extends ChannelResponse {
  @XmlElement(name = ChannelParameterType.Name.REFERENCE_NO)
  private Integer referenceNo;

  @XmlElement(name = ChannelParameterType.Name.DATE_AND_TIME)
  private Date dateAndTime;

  @XmlElement(name = ChannelParameterType.Name.ORIGIN_ACQUIRER_INSTITUTION_ID)
  private Object originAcquirerInstitutionId;
}
