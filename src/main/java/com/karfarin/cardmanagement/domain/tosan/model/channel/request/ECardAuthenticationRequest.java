package com.karfarin.cardmanagement.domain.tosan.model.channel.request;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;

import java.util.Date;

@Data
public class ECardAuthenticationRequest extends ChannelRequest {
  private String pan;
  private String pin2;
  private String cvv2;
  private Date expireDate;
  private String psdCode;
  private String acquiringTerminal;
}
