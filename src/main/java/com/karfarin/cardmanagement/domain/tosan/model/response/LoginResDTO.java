package com.karfarin.cardmanagement.domain.tosan.model.response;

import com.karfarin.cardmanagement.domain.tosan.model.SubscriberUserInfo;
import com.karfarin.cardmanagement.domain.tosan.model.enums.GenderWSType;
import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Data
public class LoginResDTO {
  private String cif;
  private String code;
  private String email;
  private GenderWSType gender;
  private Date lastLoginTime;
  private String mobile;
  private String name;
  private String refreshToken;
  private String sessionId;
  private List<SubscriberUserInfo> subscribers;
  private String temporarySessionId;
  private String title;
}
