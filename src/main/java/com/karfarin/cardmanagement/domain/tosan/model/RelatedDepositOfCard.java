package com.karfarin.cardmanagement.domain.tosan.model;

import com.karfarin.cardmanagement.domain.tosan.model.enums.CardDepositType;
import lombok.Data;

@Data
public class RelatedDepositOfCard {
  private CardDepositType cardDepositType;
  private String depositNumber;
}
