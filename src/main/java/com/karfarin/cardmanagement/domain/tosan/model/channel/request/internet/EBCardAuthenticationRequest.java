package com.karfarin.cardmanagement.domain.tosan.model.channel.request.internet;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;

import java.util.Date;

@Data
public class EBCardAuthenticationRequest extends ChannelRequest {

  private String pan;
  private String pin2;
  private String cvv2;
  private Date expireDate;
  private String psdCode;
  private String acquiringTerminal;
}
