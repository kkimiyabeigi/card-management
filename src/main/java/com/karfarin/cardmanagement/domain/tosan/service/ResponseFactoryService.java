package com.karfarin.cardmanagement.domain.tosan.service;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelResultType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.request.CardTransferRequest;
import com.karfarin.cardmanagement.domain.tosan.model.channel.request.internet.*;
import com.karfarin.cardmanagement.domain.tosan.model.channel.response.CardTransferResponse;
import com.karfarin.cardmanagement.domain.tosan.model.channel.response.internet.*;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.StringWriter;
import java.util.Date;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co
 * @date 2020/08/23 17:10 PM
 */
public class ResponseFactoryService {

  /**
   * Global converting channel request to object
   *
   * @param tosanResponse The response of Tosan
   * @param channelRequest The object of channel request
   * @param <T> The class type of Tosan response
   * @param <G> The class type of channel request
   * @return The string of channel response
   * @throws JAXBException The exception of inner method
   */
  public <T, G> String createChannelResponse(T tosanResponse, G channelRequest)
      throws JAXBException {
    if (channelRequest instanceof CardTransferRequest)
      return createCardTransferResponse((CardTransferRequest) channelRequest);
    if (channelRequest instanceof EBCardAuthenticationRequest)
      return createEBCardAuthenticationResponse((EBCardAuthenticationRequest) channelRequest);
    if (channelRequest instanceof EBFundTransferRequest)
      return createEBFundTransferResponse((EBFundTransferRequest) channelRequest);
    if (channelRequest instanceof EBTransferRequest)
      return createEBTransferResponse((EBTransferRequest) channelRequest);
    if (channelRequest instanceof EBTransferValidateRequest)
      return createEBTransferValidateResponse((EBTransferValidateRequest) channelRequest);
    if (channelRequest instanceof GetCustomerCardsByClientIDRequest)
      return createGetCustomerCardsByClientIDResponse((GetCustomerCardsByClientIDRequest) channelRequest);

    return null;
  }

  /**
   * Marshaller from object to xml string
   *
   * @param response The object of channel reponse
   * @param <T> The class of channel object
   * @return The String of xml
   * @throws JAXBException The exception of JAXBContext for newInstance
   */
  private <T> String marshalObjectToXML(T response) throws JAXBException {
    JAXBContext context = JAXBContext.newInstance(response.getClass());
    Marshaller marshaller = context.createMarshaller();
    marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE);
    StringWriter stringWriter = new StringWriter();
    marshaller.marshal(response, stringWriter);
    return stringWriter.toString();
  }

  /**
   * Create xml response for channel
   *
   * @param request The channel request
   * @return The string of xml for channel
   * @throws JAXBException The exception of marshalObjectToXML
   */
  private String createCardTransferResponse(CardTransferRequest request) throws JAXBException {
    CardTransferResponse cardTransferResponse = new CardTransferResponse();
    cardTransferResponse.setCommand(request.getCommand());
    cardTransferResponse.setResult(ChannelResultType.ACTION_OK.getName());
    cardTransferResponse.setErrorNo(0);
    cardTransferResponse.setDescription("");
    cardTransferResponse.setFarsiDescription("");
    cardTransferResponse.setJobId(request.getJobId());
    cardTransferResponse.setMessageId(request.getMessageId());
    cardTransferResponse.setReferenceNo(request.getReferenceNo());
    cardTransferResponse.setDateAndTime(new Date());
    cardTransferResponse.setOriginAcquirerInstitutionId("");

    return marshalObjectToXML(cardTransferResponse);
  }


  private String createEBCardAuthenticationResponse(EBCardAuthenticationRequest request) throws JAXBException {
    EBCardAuthenticationResponse ebCardAuthenticationResponse = new EBCardAuthenticationResponse();
    ebCardAuthenticationResponse.setCommand(request.getCommand());
    ebCardAuthenticationResponse.setResult(ChannelResultType.ACTION_OK.getName());
    ebCardAuthenticationResponse.setErrorNo(0);
    ebCardAuthenticationResponse.setDescription("");
    ebCardAuthenticationResponse.setFarsiDescription("");
    ebCardAuthenticationResponse.setJobId(request.getJobId());
    ebCardAuthenticationResponse.setMessageId(request.getMessageId());
    ebCardAuthenticationResponse.setAccountNumber("");

    return marshalObjectToXML(ebCardAuthenticationResponse);
  }

  private String createEBFundTransferResponse(EBFundTransferRequest request) throws JAXBException {
    EBFundTransferResponse ebFundTransferResponse = new EBFundTransferResponse();
    ebFundTransferResponse.setCommand(request.getCommand());
    ebFundTransferResponse.setResult(ChannelResultType.ACTION_OK.getName());
    ebFundTransferResponse.setErrorNo(0);
    ebFundTransferResponse.setDescription("");
    ebFundTransferResponse.setFarsiDescription("");
    ebFundTransferResponse.setJobId(request.getJobId());
    ebFundTransferResponse.setMessageId(request.getMessageId());
    ebFundTransferResponse.setHostDateTime(request.getSettlementDate());
    ebFundTransferResponse.setDateAndTime(new Date());
    ebFundTransferResponse.setPan(request.getPan());
    ebFundTransferResponse.setStan("");

    return marshalObjectToXML(ebFundTransferResponse);
  }

  private String createEBTransferResponse(EBTransferRequest request) throws JAXBException {
    EBTransferResponse ebTransferResponse = new EBTransferResponse();
    ebTransferResponse.setCommand(request.getCommand());
    ebTransferResponse.setResult(ChannelResultType.ACTION_OK.getName());
    ebTransferResponse.setErrorNo(0);
    ebTransferResponse.setDescription("");
    ebTransferResponse.setFarsiDescription("");
    ebTransferResponse.setJobId(request.getJobId());
    ebTransferResponse.setMessageId(request.getMessageId());
    ebTransferResponse.setReferenceNumber(request.getReferenceNumber());
    ebTransferResponse.setDateAndTime(new Date());
    ebTransferResponse.setOriginAcquirerInstitutionId("");
    ebTransferResponse.setAdditionalPrivateData("");

    return marshalObjectToXML(ebTransferResponse);
  }

  private String createEBTransferValidateResponse(EBTransferValidateRequest request) throws JAXBException {
    EBTransferValidateResponse ebTransferValidateResponse = new EBTransferValidateResponse();
    ebTransferValidateResponse.setCommand(request.getCommand());
    ebTransferValidateResponse.setResult(ChannelResultType.ACTION_OK.getName());
    ebTransferValidateResponse.setErrorNo(0);
    ebTransferValidateResponse.setDescription("");
    ebTransferValidateResponse.setFarsiDescription("");
    ebTransferValidateResponse.setJobId(request.getJobId());
    ebTransferValidateResponse.setMessageId(request.getMessageId());
    ebTransferValidateResponse.setDestinationAccount("");
    ebTransferValidateResponse.setDestinationInfo("");

    return marshalObjectToXML(ebTransferValidateResponse);
  }

  private String createGetCustomerCardsByClientIDResponse(GetCustomerCardsByClientIDRequest request) throws JAXBException {
    GetCustomerCardsByClientIDResponse getCustomerCardsByClientIDResponse = new GetCustomerCardsByClientIDResponse();
    getCustomerCardsByClientIDResponse.setCommand(request.getCommand());
    getCustomerCardsByClientIDResponse.setResult(ChannelResultType.ACTION_OK.getName());
    getCustomerCardsByClientIDResponse.setErrorNo(0);
    getCustomerCardsByClientIDResponse.setDescription("");
    getCustomerCardsByClientIDResponse.setFarsiDescription("");
    getCustomerCardsByClientIDResponse.setJobId(request.getJobId());
    getCustomerCardsByClientIDResponse.setMessageId(request.getMessageId());
    getCustomerCardsByClientIDResponse.setPan0("");
    getCustomerCardsByClientIDResponse.setPanStatus0("");
    getCustomerCardsByClientIDResponse.setPanName0("");
    getCustomerCardsByClientIDResponse.setPanType0("");
    getCustomerCardsByClientIDResponse.setPanRelationType0(0);
    getCustomerCardsByClientIDResponse.setBranch0("");
    getCustomerCardsByClientIDResponse.setPan1("");
    getCustomerCardsByClientIDResponse.setPanStatus1("");
    getCustomerCardsByClientIDResponse.setPanName1("");
    getCustomerCardsByClientIDResponse.setPanType1("");
    getCustomerCardsByClientIDResponse.setPanRelationType1(0);
    getCustomerCardsByClientIDResponse.setBranch1("");
    getCustomerCardsByClientIDResponse.setItemCount(0);

    return marshalObjectToXML(getCustomerCardsByClientIDResponse);
  }

}
