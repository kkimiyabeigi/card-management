package com.karfarin.cardmanagement.domain.tosan.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@ConfigurationProperties(prefix = "tosan")
public class TosanConfiguration {
  private String url;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }
}
