package com.karfarin.cardmanagement.domain.tosan.model.channel.response.mobile;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @authors Fereydun Rashidabadi <f.rashidabadi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = ChannelParameterType.Name.RESPONSE)
@XmlAccessorType(XmlAccessType.FIELD)
public class MBTransferValidateResponse extends ChannelResponse {
  @XmlElement(name = ChannelParameterType.Name.DESTINATION_ACCOUNT)
  private String destinationAccount;

  @XmlElement(name = ChannelParameterType.Name.DESTINATION_INFO)
  private String destinationInfo;
}
