package com.karfarin.cardmanagement.domain.tosan.model.channel.request.internet;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;

import java.util.Date;

@Data
public class EBTransferRequest extends ChannelRequest {

  private String referenceNumber;
  private String panSource;
  private String pin2;
  private String cvv2;
  private Date expireDate;
  private String panDestination;
  private String sourceAccount;
  private String destinationAccount;
  private String amount;
  private String psdCode;
  private String acquiringTerminal;
}
