package com.karfarin.cardmanagement.domain.tosan.model.request;

import javax.validation.constraints.NotNull;
import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = {"password"})
public class UserInfoReqDTO {
  private Boolean includeSubscribers;
  @NotNull private String password;
  @NotNull private String username;
}
