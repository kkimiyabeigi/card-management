package com.karfarin.cardmanagement.domain.tosan.model.channel.response;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;

@Data
public class ECardAuthenticationResponse extends ChannelResponse {
  private String sourceAccountNubmber;
  private String destinationAccountNumber;
  private String destinationAccountInfo;
}
