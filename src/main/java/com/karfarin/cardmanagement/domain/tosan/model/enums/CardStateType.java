package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum CardStateType {
  OK,
  HOT,
  WARM,
  BLOCKED,
  CAPTURED,
  EXPIRED,
  INACTIVE,
  SETTLEMENT,
  CLOSED,
  DISABLE
}
