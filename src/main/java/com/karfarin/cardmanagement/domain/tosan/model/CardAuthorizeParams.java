package com.karfarin.cardmanagement.domain.tosan.model;

import com.karfarin.cardmanagement.domain.tosan.model.enums.PinType;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CardAuthorizeParams {
  @NotNull private PinType pinType;
  private String cvv2;
  private String expDate;
  @NotNull private String pin;
  private String track2;
}
