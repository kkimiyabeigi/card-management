package com.karfarin.cardmanagement.domain.tosan.model.request;

import com.karfarin.cardmanagement.domain.tosan.model.enums.AuthenticationTypeWSType;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class OtpLoginReqDTO {
  @NotNull private AuthenticationTypeWSType authenticationType;
  private String challengeIdentifier;
  private String otpSync;
  private UserInfoReqDTO userInfoRequest;
}
