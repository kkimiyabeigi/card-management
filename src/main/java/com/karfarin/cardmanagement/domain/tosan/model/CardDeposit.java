package com.karfarin.cardmanagement.domain.tosan.model;

import lombok.Data;

@Data
public class CardDeposit {
  private Amount availableBalance;
  private Amount ledgerBalance;
}
