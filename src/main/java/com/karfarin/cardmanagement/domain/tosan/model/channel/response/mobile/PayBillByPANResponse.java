package com.karfarin.cardmanagement.domain.tosan.model.channel.response.mobile;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * @authors Fereydun Rashidabadi <f.rashidabadi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = ChannelParameterType.Name.RESPONSE)
@XmlAccessorType(XmlAccessType.FIELD)
public class PayBillByPANResponse extends ChannelResponse {
  @XmlElement(name = ChannelParameterType.Name.COMPANY_NAME)
  private String companyName;

  @XmlElement(name = ChannelParameterType.Name.COMPANY_FARSI_NAME)
  private String companyFarsiName;

  @XmlElement(name = ChannelParameterType.Name.SERVICE_ID)
  private String serviceID;

  @XmlElement(name = ChannelParameterType.Name.AMOUNT)
  private String amount;

  @XmlElement(name = ChannelParameterType.Name.REFERENCE_NO)
  private String referenceNo;

  @XmlElement(name = ChannelParameterType.Name.PERSIAN_DATE)
  private String persianDate;

  @XmlElement(name = ChannelParameterType.Name.DATE)
  private Date date;

  @XmlElement(name = ChannelParameterType.Name.BILL_CODE)
  private Date billCode;

  @XmlElement(name = ChannelParameterType.Name.PAYMENT_CODE)
  private Date paymentCode;

  @XmlElement(name = ChannelParameterType.Name.CHANNEL)
  private Date channel;
}
