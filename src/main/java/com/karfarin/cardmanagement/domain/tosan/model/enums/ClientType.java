package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum ClientType {
  ATM,
  POS,
  PINPAD,
  INTERNET,
  VRU,
  POSCONDCOD,
  PHONE,
  OTHER;
}
