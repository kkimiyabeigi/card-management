package com.karfarin.cardmanagement.domain.tosan.model.request;

import com.karfarin.cardmanagement.domain.tosan.model.enums.ClientType;
import javax.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class IssuerCustomReturnReqDTO {
  @NotNull private BigDecimal amount;
  @NotNull private ClientType clientType;
  @NotNull private String merchantId;
  @NotNull private String referenceNumber;
  @NotNull private String resNum;
  @NotNull private String terminalId;
}
