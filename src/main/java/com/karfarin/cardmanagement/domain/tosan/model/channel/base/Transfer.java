package com.karfarin.cardmanagement.domain.tosan.model.channel.base;

import lombok.Data;

import java.util.Date;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/22 11:30 AM
 */
@Data
public class Transfer extends ChannelRequest {

  private Integer referenceNo;
  private String panSource;
  private String pin2;
  private String cvv2;
  private Date expireDate;
  private String panDestination;
  private String sourceAccount;
  private String destinationAccount;
  private String amount;
  private String psdCode;
  private String acquiringTerminal;
}
