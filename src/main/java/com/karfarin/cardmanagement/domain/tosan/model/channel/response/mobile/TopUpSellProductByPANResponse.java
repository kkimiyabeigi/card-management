package com.karfarin.cardmanagement.domain.tosan.model.channel.response.mobile;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.math.BigDecimal;

/**
 * @authors Fereydun Rashidabadi <f.rashidabadi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = ChannelParameterType.Name.RESPONSE)
@XmlAccessorType(XmlAccessType.FIELD)
public class TopUpSellProductByPANResponse extends ChannelResponse {
  @XmlElement(name = ChannelParameterType.Name.REFERENCE_ID)
  private String refID;

  @XmlElement(name = ChannelParameterType.Name.RESELLER_REFERENCE_CODE)
  private String resellerRefCode;

  @XmlElement(name = ChannelParameterType.Name.COMPANY_CODE)
  private String companyCode;

  @XmlElement(name = ChannelParameterType.Name.COMPANY_FARSI_NAME)
  private String companyFName;

  @XmlElement(name = ChannelParameterType.Name.PRODUCT_CODE)
  private String productCode;

  @XmlElement(name = ChannelParameterType.Name.PRODUCT_NAME)
  private String productName;

  @XmlElement(name = ChannelParameterType.Name.PLAN_CODE)
  private String planCode;

  @XmlElement(name = ChannelParameterType.Name.PLAN_NAME)
  private String planName;

  @XmlElement(name = ChannelParameterType.Name.AMOUNT)
  private BigDecimal amount;

  @XmlElement(name = ChannelParameterType.Name.MOBILE_NUMBER)
  private String mobileNumber;
}
