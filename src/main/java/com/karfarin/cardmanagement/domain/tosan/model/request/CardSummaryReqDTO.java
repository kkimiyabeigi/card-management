package com.karfarin.cardmanagement.domain.tosan.model.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CardSummaryReqDTO {
  @NotNull private String cardPan;
  private Boolean showIban;
}
