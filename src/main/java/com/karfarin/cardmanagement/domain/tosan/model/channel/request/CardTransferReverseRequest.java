package com.karfarin.cardmanagement.domain.tosan.model.channel.request;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;

@Data
@EqualsAndHashCode(callSuper = true)
public class CardTransferReverseRequest extends ChannelRequest {
  private String psdCode;
  private String acquiringTerminal;
  private String panSource;
  private String panDestination;
  private BigDecimal amount;
  private String referenceNumber;
}
