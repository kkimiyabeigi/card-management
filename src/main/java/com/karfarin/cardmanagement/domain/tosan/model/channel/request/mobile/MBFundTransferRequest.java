package com.karfarin.cardmanagement.domain.tosan.model.channel.request.mobile;

import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelRequest;
import lombok.Data;

import java.util.Date;

/**
 * @authors Fereydun Rashidabadi <f.rashidabadi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Data
public class MBFundTransferRequest extends ChannelRequest {

  private String clientNo;
  private String amount;
  private String accountNumber;
  private String accountNumberDest;
  private String recInstID;
  private Date settlementDate;
  private String acquiringTerminal;
  private String PAN;
}
