package com.karfarin.cardmanagement.domain.tosan.model;

import lombok.Data;

@Data
public class ContextEntry {
  private String key;
  private String value;
}
