package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum DepositOrPanWSType {
  PAN,
  DEPOSIT;
}
