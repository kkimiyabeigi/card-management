package com.karfarin.cardmanagement.domain.tosan.model.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ChangeCardSecondPasswordReqDTO {
  private String cvv2;
  private String expDate;
  @NotNull private String newPin;
  @NotNull private String pan;
  @NotNull private String pin;
  private String track2;
}
