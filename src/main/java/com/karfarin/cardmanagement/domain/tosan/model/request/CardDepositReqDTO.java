package com.karfarin.cardmanagement.domain.tosan.model.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CardDepositReqDTO {
  private String cif;
  @NotNull private String pan;
}
