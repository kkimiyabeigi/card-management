package com.karfarin.cardmanagement.domain.tosan.model.request;

import com.karfarin.cardmanagement.domain.tosan.model.enums.DepositOrPanWSType;
import com.karfarin.cardmanagement.domain.tosan.model.enums.PinType;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@NoArgsConstructor
public class FundTransferReqDTO {
  @NotNull
  private BigDecimal amount;
  private String cvv2;
  @NotNull private String destination;
  @NotNull private DepositOrPanWSType destinationType;
  private String expDate;
  private String merchantId;
  @NotNull private String pan;
  @NotNull private String pin;
  @NotNull private PinType pinType;
  private String pspCode;
  private String track2;
  private String twoPhaseReferenceCode;
}
