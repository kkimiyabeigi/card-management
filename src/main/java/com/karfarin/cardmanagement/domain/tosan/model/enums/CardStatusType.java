package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum CardStatusType {
  OK,
  HOT,
  WARM,
  BLOCKED,
  CAPTURED,
  EXPIRED,
  INACTIVE,
  SETTLEMENT,
  CLOSED;
}
