package com.karfarin.cardmanagement.domain.tosan.model.request;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class ActivateCardOtpTokenReqDTO {
  @NotNull private String cif;
  @NotNull private String cipherMessage;
  private Boolean generateFirstOtp;
  private Boolean generateSecondOtp;
  @NotNull private String publicKey;
}
