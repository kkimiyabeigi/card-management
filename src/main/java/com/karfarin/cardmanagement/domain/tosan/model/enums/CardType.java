package com.karfarin.cardmanagement.domain.tosan.model.enums;

public enum CardType {
  DEBIT,
  CREDIT,
  WALLET,
  PREPAID
}
