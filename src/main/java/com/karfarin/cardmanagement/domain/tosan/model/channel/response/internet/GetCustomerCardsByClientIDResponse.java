package com.karfarin.cardmanagement.domain.tosan.model.channel.response.internet;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = ChannelParameterType.Name.RESPONSE)
@XmlAccessorType(XmlAccessType.FIELD)
public class GetCustomerCardsByClientIDResponse extends ChannelResponse {

    @XmlElement(name = ChannelParameterType.Name.PAN_0)
    private String pan0;

    @XmlElement(name = ChannelParameterType.Name.PAN_STATUS_0)
    private String panStatus0;

    @XmlElement(name = ChannelParameterType.Name.PAN_NAME_0)
    private String panName0;

    @XmlElement(name = ChannelParameterType.Name.PAN_TYPE_0)
    private String panType0;

    @XmlElement(name = ChannelParameterType.Name.PAN_RELATION_TYPE_0)
    private Integer panRelationType0;

    @XmlElement(name = ChannelParameterType.Name.BRANCH_0)
    private String branch0;

    @XmlElement(name = ChannelParameterType.Name.PAN_1)
    private String pan1;

    @XmlElement(name = ChannelParameterType.Name.PAN_STATUS_1)
    private String panStatus1;

    @XmlElement(name = ChannelParameterType.Name.PAN_NAME_1)
    private String panName1;

    @XmlElement(name = ChannelParameterType.Name.PAN_TYPE_1)
    private String panType1;

    @XmlElement(name = ChannelParameterType.Name.PAN_RELATION_TYPE_1)
    private Integer panRelationType1;

    @XmlElement(name = ChannelParameterType.Name.BRANCH_1)
    private String branch1;

    @XmlElement(name = ChannelParameterType.Name.ITEM_COUNT)
    private Integer itemCount;

}
