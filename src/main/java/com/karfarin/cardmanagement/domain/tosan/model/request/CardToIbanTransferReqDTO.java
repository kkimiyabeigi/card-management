package com.karfarin.cardmanagement.domain.tosan.model.request;

import com.karfarin.cardmanagement.domain.tosan.model.enums.PinType;
import javax.validation.constraints.NotNull;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CardToIbanTransferReqDTO {
  @NotNull private BigDecimal amount;
  private String cvv2;
  private String expDate;
  @NotNull private String ibanNumber;
  @NotNull private String pan;
  @NotNull private String pin;
  @NotNull private PinType pinType;
  private String track2;
  private String twoPhaseReferenceCode;
}
