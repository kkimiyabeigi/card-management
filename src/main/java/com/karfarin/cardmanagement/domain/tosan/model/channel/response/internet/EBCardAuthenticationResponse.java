package com.karfarin.cardmanagement.domain.tosan.model.channel.response.internet;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.domain.tosan.model.channel.base.ChannelResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@XmlRootElement(name = ChannelParameterType.Name.RESPONSE)
@XmlAccessorType(XmlAccessType.FIELD)
public class EBCardAuthenticationResponse extends ChannelResponse {

  @XmlElement(name = ChannelParameterType.Name.ACCOUNT_NUMBER_RESPONSE)
  private String accountNumber;
}
