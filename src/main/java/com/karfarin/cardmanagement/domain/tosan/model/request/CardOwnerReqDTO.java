package com.karfarin.cardmanagement.domain.tosan.model.request;

import com.karfarin.cardmanagement.domain.tosan.model.CardAuthorizeParams;
import lombok.Data;

@Data
public class CardOwnerReqDTO {
  private CardAuthorizeParams cardAuthorizeParams;
  private String destinationPan;
  private String loanNumber;
  private String pan;
}
