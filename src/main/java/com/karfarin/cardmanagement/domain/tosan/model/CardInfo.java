package com.karfarin.cardmanagement.domain.tosan.model;

import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class CardInfo {
  private String cvv2;
  private String expireDate;
  @NotNull private String pan;
  private String pin2;
}
