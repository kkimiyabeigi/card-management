package com.karfarin.cardmanagement.domain.tosan.model.request;

import com.karfarin.cardmanagement.domain.tosan.model.CardInfo;
import com.karfarin.cardmanagement.domain.tosan.model.enums.ProcessType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class HarimOtpReqDTO {
  private BigDecimal amount;
  @NotNull private CardInfo cardInfo;
  private String merchantId;
  @NotNull private ProcessType processType;
}
