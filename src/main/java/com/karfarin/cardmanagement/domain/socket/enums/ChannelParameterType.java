package com.karfarin.cardmanagement.domain.socket.enums;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co
 * @date 2020/08/23 09:46 AM
 */
public enum ChannelParameterType {
  ADDITIONAL_PRIVATE_DATA(Name.ADDITIONAL_PRIVATE_DATA),
  CONTENT(Name.CONTENT),
  RESPONSE(Name.RESPONSE),
  RESULT(Name.RESULT),
  ERROR_NO(Name.ERROR_NO),
  DESCRIPTION(Name.DESCRIPTION),
  FARSI_DESCRIPTION(Name.FARSI_DESCRIPTION),
  COMMAND(Name.COMMAND),
  APPLICATION_NAME(Name.APPLICATION_NAME),
  JOB_ID(Name.JOB_ID),
  MESSAGE_ID(Name.MESSAGE_ID),
  BRANCH_ID(Name.BRANCH_ID),
  BRANCH_0(Name.BRANCH_0),
  BRANCH_1(Name.BRANCH_1),
  USER_ID(Name.USER_ID),
  REFERENCE_NO(Name.REFERENCE_NO),
  REFERENCE_NO_RESPONSE(Name.REFERENCE_NO_RESPONSE),
  PAN_0(Name.PAN_0),
  PAN_STATUS_0(Name.PAN_STATUS_0),
  PAN_NAME_0(Name.PAN_NAME_0),
  PAN_TYPE_0(Name.PAN_TYPE_0),
  PAN_RELATION_TYPE_0(Name.PAN_RELATION_TYPE_0),
  PAN_1(Name.PAN_1),
  PAN_STATUS_1(Name.PAN_STATUS_1),
  PAN_NAME_1(Name.PAN_NAME_1),
  PAN_TYPE_1(Name.PAN_TYPE_1),
  PAN_RELATION_TYPE_1(Name.PAN_RELATION_TYPE_1),
  PAN_SOURCE(Name.PAN_SOURCE),
  PAN_DESTINATION(Name.PAN_DESTINATION),
  PIN_2(Name.PIN_2),
  CVV_2(Name.CVV_2),
  EXPIRE_DATE(Name.EXPIRE_DATE),
  SOURCE_ACCOUNT(Name.SOURCE_ACCOUNT),
  DESTINATION_ACCOUNT(Name.DESTINATION_ACCOUNT),
  DESTINATION_ACCOUNT_RESPONSE(Name.DESTINATION_ACCOUNT_RESPONSE),
  AMOUNT(Name.AMOUNT),
  PSD_CODE(Name.PSD_CODE),
  ACQUIRING_TERMINAL(Name.ACQUIRING_TERMINAL),
  SETTLEMENT_DATE(Name.SETTLEMENT_DATE),
  MOBILE_NUMBER(Name.MOBILE_NUMBER),
  IP(Name.IP),
  REQUEST(Name.REQUEST),
  DATE_AND_TIME(Name.DATE_AND_TIME),
  DATE_AND_TIME_RESPONSE(Name.DATE_AND_TIME_RESPONSE),
  HOST_DATE_AND_TIME(Name.HOST_DATE_AND_TIME),
  ORIGIN_ACQUIRER_INSTITUTION_ID(Name.ORIGIN_ACQUIRER_INSTITUTION_ID),
  ORIGIN_ACQUIRER_INSTITUTION_ID_RESPONSE(Name.ORIGIN_ACQUIRER_INSTITUTION_ID_RESPONSE),
  REC_INST_ID(Name.REC_INST_ID),
  PAYMENT_ID(Name.PAYMENT_ID),
  CR_PAYMENT_ID(Name.CR_PAYMENT_ID),
  DESTINATION_DESC(Name.DESTINATION_DESC),
  PAN(Name.PAN),
  PAN_RESPONSE(Name.PAN_RESPONSE),
  CLIENT_NO(Name.CLIENT_NO),
  CUSTOMER_NUMBER(Name.CUSTOMER_NUMBER),
  ACCOUNT_TYPE(Name.ACCOUNT_TYPE),
  DEVICE(Name.DEVICE),
  ACCOUNT_NUMBER(Name.ACCOUNT_NUMBER),
  ACCOUNT_NUMBER2(Name.ACCOUNT_NUMBER2),
  ACCOUNT_NUMBER_DEST(Name.ACCOUNT_NUMBER_DEST),
  ACCOUNT_NUMBER_RESPONSE(Name.ACCOUNT_NUMBER_RESPONSE),
  ACCOUNT_NUMBER_DESTINATION(Name.ACCOUNT_NUMBER_DESTINATION),
  RECORD_INSERT_ID(Name.RECORD_INSERT_ID),
  NEW_PIN_2(Name.NEW_PIN_2),
  NATIONAL_ID(Name.NATIONAL_ID),
  STAN(Name.STAN),
  STAN_RESPONSE(Name.STAN_RESPONSE),
  OTP_CHANNEL(Name.OTP_CHANNEL),
  OTP_TYPE(Name.OTP_TYPE),
  CHARGE(Name.CHARGE),
  VERIFICATION_CODE(Name.VERIFICATION_CODE),
  BILL_CODE(Name.BILL_CODE),
  CHANNEL(Name.CHANNEL),
  STAN_CARD(Name.STAN_CARD),
  USER_APP(Name.USER_APP),
  INITIATOR_DATE_TIME(Name.INITIATOR_DATE_TIME),
  ACQUIRING_TELLER_ID_TERMINAL(Name.ACQUIRING_TELLER_ID_TERMINAL),
  DEVICE_CODE(Name.DEVICE_CODE),
  COMPANY_CODE(Name.COMPANY_CODE),
  PLAN_CODE(Name.PLAN_CODE),
  PRODUCT_CODE(Name.PRODUCT_CODE),
  INF_OF_41(Name.INF_OF_41),
  TELLER_ID(Name.TELLER_ID),
  TIMESTAMP(Name.TIMESTAMP),
  DESTINATION_INFO(Name.DESTINATION_INFO);

  public class Name {

    public static final String ADDITIONAL_PRIVATE_DATA ="additionalprivatedata";
    public static final String CONTENT = "content";
    public static final String RESPONSE = "response";
    public static final String RESULT = "result";
    public static final String ERROR_NO = "errorno";
    public static final String DESCRIPTION = "desc";
    public static final String FARSI_DESCRIPTION = "fdesc";
    public static final String COMMAND = "cmd";
    public static final String APPLICATION_NAME = "appname";
    public static final String JOB_ID = "jobid";
    public static final String MESSAGE_ID = "msgid";
    public static final String BRANCH_ID = "branchid";
    public static final String BRANCH_0 = "branch_0";
    public static final String BRANCH_1 = "branch_1";
    public static final String USER_ID = "userid";
    public static final String REFERENCE_NO = "RefNo";
    public static final String REFERENCE_NO_RESPONSE = "refno";
    public static final String PAN_0 = "pan_0";
    public static final String PAN_STATUS_0 = "panstatus_0";
    public static final String PAN_NAME_0 = "panname_0";
    public static final String PAN_TYPE_0 = "pantype_0";
    public static final String PAN_RELATION_TYPE_0 = "panrelationtype_0";
    public static final String PAN_1 = "pan_1";
    public static final String PAN_STATUS_1 = "panstatus_1";
    public static final String PAN_NAME_1 = "panname_1";
    public static final String PAN_TYPE_1 = "pantype_1";
    public static final String PAN_RELATION_TYPE_1 = "panrelationtype_1";
    public static final String PAN_SOURCE = "PANSrc";
    public static final String PAN_DESTINATION = "PANDst";
    public static final String PIN_2 = "PIN2";
    public static final String CVV_2 = "CVV2";
    public static final String EXPIRE_DATE = "ExpireDate";
    public static final String SOURCE_ACCOUNT = "SrcAccount";
    public static final String DESTINATION_ACCOUNT = "DstAccount";
    public static final String DESTINATION_ACCOUNT_RESPONSE = "dstaccount";
    public static final String AMOUNT = "Amount";
    public static final String PSD_CODE = "PSDCode";
    public static final String ACQUIRING_TERMINAL = "AcquiringTerminal";
    public static final String SETTLEMENT_DATE = "SettlementDate";
    public static final String MOBILE_NUMBER = "MobileNumber";
    public static final String IP = "IP";
    public static final String REQUEST = "request";
    public static final String DATE_AND_TIME = "DateAndTime";
    public static final String DATE_AND_TIME_RESPONSE = "dateandtime";
    public static final String HOST_DATE_AND_TIME = "hostdatetime";
    public static final String ORIGIN_ACQUIRER_INSTITUTION_ID = "OriginAcquirerInstitutionId";
    public static final String ORIGIN_ACQUIRER_INSTITUTION_ID_RESPONSE = "originacquirerinstitutionid";
    public static final String REC_INST_ID = "RecInstID";
    public static final String PAYMENT_ID = "PaymentId";
    public static final String CR_PAYMENT_ID = "CrPaymentId";
    public static final String DESTINATION_DESC = "DestinationDesc";
    public static final String PAN = "PAN";
    public static final String PAN_RESPONSE = "pan";
    public static final String CLIENT_NO = "clientNo";
    public static final String CUSTOMER_NUMBER = "customerNumber";
    public static final String ACCOUNT_TYPE = "accountType";
    public static final String DEVICE = "device";
    public static final String ACCOUNT_NUMBER = "accountNumber";
    public static final String ACCOUNT_NUMBER2 = "AccountNumber";
    public static final String ACCOUNT_NUMBER_DEST = "AccountNumberDest";
    public static final String ACCOUNT_NUMBER_RESPONSE = "accountnumber";
    public static final String ACCOUNT_NUMBER_DESTINATION = "accountNumberDest";
    public static final String RECORD_INSERT_ID = "recInstID";
    public static final String NEW_PIN_2 = "newPIN2";
    public static final String NATIONAL_ID = "nationalId";
    public static final String STAN = "STAN";
    public static final String STAN_RESPONSE = "stan";
    public static final String OTP_CHANNEL = "OTPChannel";
    public static final String OTP_TYPE = "OTPType";
    public static final String CHARGE = "charge";
    public static final String VERIFICATION_CODE = "verificationCode";
    public static final String BILL_CODE = "billcode";
    public static final String CHANNEL = "channel";
    public static final String STAN_CARD = "STANCard";
    public static final String USER_APP = "userApp";
    public static final String INITIATOR_DATE_TIME = "initiatorDateTime";
    public static final String ACQUIRING_TELLER_ID_TERMINAL = "acquiringTellerIdTerminal";
    public static final String DEVICE_CODE = "deviceCode";
    public static final String COMPANY_CODE = "companyCode";
    public static final String PLAN_CODE = "planCode";
    public static final String PRODUCT_CODE = "productCode";
    public static final String INF_OF_41 = "infof41";
    public static final String TELLER_ID = "tellerId";
    public static final String TIMESTAMP = "timestamp";
    public static final String REFERENCE_ID = "refId";
    public static final String RESELLER_REFERENCE_CODE = "resellerRefCode";
    public static final String COMPANY_FARSI_NAME = "companyFName";
    public static final String PRODUCT_NAME = "productName";
    public static final String PLAN_NAME = "planName";
    public static final String ACCOUNT_NO = "accno";
    public static final String ACCOUNT_RELATION_TYPE = "accrelationtype";
    public static final String ITEM_COUNT = "itemcount";
    public static final String PAN_STATUS = "panstatus";
    public static final String COMPANY_NAME = "panname";
    public static final String PAN_TYPE = "pantype";
    public static final String PAN_RELATION_TYPE = "panrelationtype";
    public static final String HOST_DATE_TIME = "hostdatetime";
    public static final String OPEN_STATUS = "openstatus";
    public static final String ACCOUNT_REMAIN = "accountremain";
    public static final String DESTINATION_INFO = "dstinfo";
    public static final String SERVICE_ID = "idservice";
    public static final String PERSIAN_DATE = "pdate";
    public static final String DATE = "date";
    public static final String PAYMENT_CODE = "paycode";
  }

  private final String label;

  ChannelParameterType(String label) {
    this.label = label;
  }

  public String toString() {
    return label;
  }
}
