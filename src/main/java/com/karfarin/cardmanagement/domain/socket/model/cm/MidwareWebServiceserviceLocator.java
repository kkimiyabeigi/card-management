/**
 * MidwareWebServiceserviceLocator.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT)
 * WSDL2Java emitter.
 */
package com.karfarin.cardmanagement.domain.socket.model.cm;

public class MidwareWebServiceserviceLocator extends org.apache.axis.client.Service
    implements MidwareWebServiceservice {

  public MidwareWebServiceserviceLocator() {}

  public MidwareWebServiceserviceLocator(org.apache.axis.EngineConfiguration config) {
    super(config);
  }

  public MidwareWebServiceserviceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
      throws javax.xml.rpc.ServiceException {
    super(wsdlLoc, sName);
  }

  // Use to get a proxy class for MidwareWebServicePort
  private java.lang.String MidwareWebServicePort_address =
      "http://10.5.16.24:447/soap/MidwareWebService";

  public java.lang.String getMidwareWebServicePortAddress() {
    return MidwareWebServicePort_address;
  }

  // The WSDD service name defaults to the port name.
  private java.lang.String MidwareWebServicePortWSDDServiceName = "MidwareWebServicePort";

  public java.lang.String getMidwareWebServicePortWSDDServiceName() {
    return MidwareWebServicePortWSDDServiceName;
  }

  public void setMidwareWebServicePortWSDDServiceName(java.lang.String name) {
    MidwareWebServicePortWSDDServiceName = name;
  }

  public MidwareWebService getMidwareWebServicePort() throws javax.xml.rpc.ServiceException {
    java.net.URL endpoint;
    try {
      endpoint = new java.net.URL(MidwareWebServicePort_address);
    } catch (java.net.MalformedURLException e) {
      throw new javax.xml.rpc.ServiceException(e);
    }
    return getMidwareWebServicePort(endpoint);
  }

  public MidwareWebService getMidwareWebServicePort(java.net.URL portAddress)
      throws javax.xml.rpc.ServiceException {
    try {
      MidwareWebServicebindingStub _stub = new MidwareWebServicebindingStub(portAddress, this);
      _stub.setPortName(getMidwareWebServicePortWSDDServiceName());
      return _stub;
    } catch (org.apache.axis.AxisFault e) {
      return null;
    }
  }

  public void setMidwareWebServicePortEndpointAddress(java.lang.String address) {
    MidwareWebServicePort_address = address;
  }

  /**
   * For the given interface, get the stub implementation. If this service has no port for the given
   * interface, then ServiceException is thrown.
   */
  public java.rmi.Remote getPort(Class serviceEndpointInterface)
      throws javax.xml.rpc.ServiceException {
    try {
      if (MidwareWebService.class.isAssignableFrom(serviceEndpointInterface)) {
        MidwareWebServicebindingStub _stub =
            new MidwareWebServicebindingStub(new java.net.URL(MidwareWebServicePort_address), this);
        _stub.setPortName(getMidwareWebServicePortWSDDServiceName());
        return _stub;
      }
    } catch (java.lang.Throwable t) {
      throw new javax.xml.rpc.ServiceException(t);
    }
    throw new javax.xml.rpc.ServiceException(
        "There is no stub implementation for the interface:  "
            + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
  }

  /**
   * For the given interface, get the stub implementation. If this service has no port for the given
   * interface, then ServiceException is thrown.
   */
  public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
      throws javax.xml.rpc.ServiceException {
    if (portName == null) {
      return getPort(serviceEndpointInterface);
    }
    java.lang.String inputPortName = portName.getLocalPart();
    if ("MidwareWebServicePort".equals(inputPortName)) {
      return getMidwareWebServicePort();
    } else {
      java.rmi.Remote _stub = getPort(serviceEndpointInterface);
      ((org.apache.axis.client.Stub) _stub).setPortName(portName);
      return _stub;
    }
  }

  public javax.xml.namespace.QName getServiceName() {
    return new javax.xml.namespace.QName("http://tempuri.org/", "MidwareWebServiceservice");
  }

  private java.util.HashSet ports = null;

  public java.util.Iterator getPorts() {
    if (ports == null) {
      ports = new java.util.HashSet();
      ports.add(new javax.xml.namespace.QName("http://tempuri.org/", "MidwareWebServicePort"));
    }
    return ports.iterator();
  }

  /** Set the endpoint address for the specified port name. */
  public void setEndpointAddress(java.lang.String portName, java.lang.String address)
      throws javax.xml.rpc.ServiceException {

    if ("MidwareWebServicePort".equals(portName)) {
      setMidwareWebServicePortEndpointAddress(address);
    } else { // Unknown Port Name
      throw new javax.xml.rpc.ServiceException(
          " Cannot set Endpoint Address for Unknown Port" + portName);
    }
  }

  /** Set the endpoint address for the specified port name. */
  public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
      throws javax.xml.rpc.ServiceException {
    setEndpointAddress(portName.getLocalPart(), address);
  }
}
