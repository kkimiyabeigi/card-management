package com.karfarin.cardmanagement.domain.socket.enums;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co
 * @date 2020/08/23 09:46 AM
 */
public enum ChannelCommandType {
  CARD_TRANSFER(Name.CARD_TRANSFER),
  EB_GET_CUSTOMER_CONTACT(Name.EB_GET_CUSTOMER_CONTACT),
  EB_CARD_AUTHENTICATION(Name.EB_CARD_AUTHENTICATION),
  EB_FUND_TRANSFER(Name.EB_FUND_TRANSFER),
  EB_TRANSFER(Name.EB_TRANSFER),
  EB_TRANSFER_VALIDATE(Name.EB_TRANSFER_VALIDATE),
  GET_ACCOUNTS_BY_PAN(Name.GET_ACCOUNTS_BY_PAN),
  MB_CARD_AUTHENTICATION(Name.MB_CARD_AUTHENTICATION),
  MB_CARD_BLOCK(Name.MB_CARD_BLOCK),
  MB_FUND_TRANSFER(Name.MB_FUND_TRANSFER),
  MB_GET_ACCOUNT_INFO(Name.MB_GET_ACCOUNT_INFO),
  GET_CUSTOMER_CARDS_BY_CLIENT_ID(Name.GET_CUSTOMER_CARDS_BY_CLIENT_ID),
  MB_SET_CARD_PIN2(Name.MB_SET_CARD_PIN2),
  MB_TRANSFER(Name.MB_TRANSFER),
  MB_TRANSFER_VALIDATE(Name.MB_TRANSFER_VALIDATE),
  OTP_GET_MOBILE_INFO(Name.OTP_GET_MOBILE_INFO),
  OTP_MATCH_MOBILE_INFO(Name.OTP_MATCH_MOBILE_INFO),
  OTP_PROVISIONING(Name.OTP_PROVISIONING),
  OTP_REQUEST_INTERNAL(Name.OTP_REQUEST_INTERNAL),
  OTP_VERIFY_MOBILE_INFO(Name.OTP_VERIFY_MOBILE_INFO),
  PAY_BILL_BY_PAN(Name.PAY_BILL_BY_PAN),
  SELL_PRODUCT_BY_PAN(Name.SELL_PRODUCT_BY_PAN),
  TOP_UP_SELL_PRODUCT_BY_PAN(Name.TOP_UP_SELL_PRODUCT_BY_PAN),
  VERIFY_V_CODE(Name.VERIFY_V_CODE),
  GET_CUSTOMER_CARDS_BY_ACC_NO_REQUEST(Name.GET_CUSTOMER_CARDS_BY_ACC_NO_REQUEST),
  DELIVERED_NOTIFICATIONS(Name.DELIVERED_NOTIFICATIONS);

  public class Name {
    public static final String CARD_TRANSFER = "CardTransfer";
    public static final String EB_GET_CUSTOMER_CONTACT = "EBGetCustomerContact";
    public static final String EB_CARD_AUTHENTICATION = "EBCardAuthentication";
    public static final String EB_FUND_TRANSFER = "EBFundTransfer";
    public static final String EB_TRANSFER = "EBTransfer";
    public static final String EB_TRANSFER_VALIDATE = "EBTransferValidate";
    public static final String GET_ACCOUNTS_BY_PAN = "GetAccountsByPAN";
    public static final String GET_CUSTOMER_CARDS_BY_CLIENT_ID = "GetCustomerCardsByClientID";
    public static final String MB_CARD_AUTHENTICATION = "MBCardAuthentication";
    public static final String MB_CARD_BLOCK = "MBCardBlock";
    public static final String MB_FUND_TRANSFER = "MBFundTransfer";
    public static final String MB_GET_ACCOUNT_INFO = "MBGetAccountInfo";
    public static final String MB_SET_CARD_PIN2 = "MBSetCardPIN2";
    public static final String MB_TRANSFER = "MBTransfer";
    public static final String MB_TRANSFER_VALIDATE = "MBTransferValidate";
    public static final String OTP_GET_MOBILE_INFO = "OtpGetMobileInfo";
    public static final String OTP_MATCH_MOBILE_INFO = "OtpMatchMobileInfo";
    public static final String OTP_PROVISIONING = "OtpProvisioning";
    public static final String OTP_REQUEST_INTERNAL = "OtpRequestInternal";
    public static final String OTP_VERIFY_MOBILE_INFO = "OtpVerifyMobileInfo";
    public static final String PAY_BILL_BY_PAN = "PayBillByPAN";
    public static final String SELL_PRODUCT_BY_PAN = "SellProductByPAN";
    public static final String TOP_UP_SELL_PRODUCT_BY_PAN = "TopUpSellProductByPAN";
    public static final String VERIFY_V_CODE = "VerifyVCode";
    public static final String GET_CUSTOMER_CARDS_BY_ACC_NO_REQUEST =
        "GetCustomerCardsByAccNoRequest";
    public static final String DELIVERED_NOTIFICATIONS = "DeliveredNotifications";
  }

  private String label;

  ChannelCommandType(String label) {
    this.label = label;
  }

  public String toString() {
    return label;
  }
}
