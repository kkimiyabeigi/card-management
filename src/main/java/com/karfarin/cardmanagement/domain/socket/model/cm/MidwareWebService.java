/**
 * MidwareWebService.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT)
 * WSDL2Java emitter.
 */
package com.karfarin.cardmanagement.domain.socket.model.cm;

public interface MidwareWebService extends java.rmi.Remote {
  public String executeCommand(String request) throws java.rmi.RemoteException;

  public long[] sendSms(
      String appName, String[] messageBodies, String[] recipientNumbers, String[] priorities)
      throws java.rmi.RemoteException;

  public int[] getRealMessageStatuses(long[] messagesId) throws java.rmi.RemoteException;
}
