package com.karfarin.cardmanagement.domain.socket.enums;

public enum ChannelResultType {
  ACTION_OK("ActionOk"),
  ACTION_FAIL("ActionFail"),
  BAD_PARAMS("BadParams");

  private String name;

  ChannelResultType(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }
}
