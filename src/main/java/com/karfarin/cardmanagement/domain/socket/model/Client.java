package com.karfarin.cardmanagement.domain.socket.model;

import lombok.Data;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/05 09:46 AM
 */
@Data
public class Client {
  private String name;
  private String ip;
  private String charset;

  /**
   * Constructor of class
   *
   * @param name The name of client
   * @param ip The ip of client
   * @param charset The character set of client
   */
  public Client(String name, String ip, String charset) {
    this.name = name;
    this.ip = ip;
    this.charset = charset;
  }
}
