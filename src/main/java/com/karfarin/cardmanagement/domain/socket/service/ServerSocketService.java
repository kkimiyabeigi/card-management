package com.karfarin.cardmanagement.domain.socket.service;

import com.karfarin.cardmanagement.domain.socket.config.TCPConfiguration;
import com.karfarin.cardmanagement.domain.socket.model.Client;
import com.karfarin.cardmanagement.domain.socket.model.ClientHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co
 * @date 2020/08/05 09:46 AM
 */
@Service
public class ServerSocketService {

  private final Logger LOG = LoggerFactory.getLogger(getClass().getName());
  private final TCPConfiguration tcpConfiguration;

  /**
   * Constructor of class
   *
   * @param tcpConfiguration dependency injection
   */
  public ServerSocketService(TCPConfiguration tcpConfiguration) {
    this.tcpConfiguration = tcpConfiguration;
  }

  /**
   * Main service of server socket
   *
   * @throws IOException exception of socket
   */
  public void startServerSocket() throws IOException {
    ServerSocket serverSocket = new ServerSocket(tcpConfiguration.getPort());
    Socket socket;

    while (true) {
      socket = serverSocket.accept();
      InetAddress clientInfo = getClientInfo(socket);

      LOG.info(
          "New client with name: {}, IP: {} send request to receive socket {} ",
          clientInfo.getHostName(),
          clientInfo.getHostAddress(),
          socket);

      Client client =
          new Client(
              clientInfo.getHostName(),
              clientInfo.getHostAddress(),
              getCharacterSet(clientInfo.getHostAddress()));
      ClientHandler clientHandler =
          new ClientHandler(client, socket, tcpConfiguration.getClients());
      Thread clientThread = new Thread(clientHandler);
      clientThread.start();

      LOG.info("New client create socket via {} ", client);
    }
  }

  /**
   * Getting client info
   *
   * @param socket the socket of client
   * @return the client info
   */
  public InetAddress getClientInfo(Socket socket) {
    InetSocketAddress clientAddress = (InetSocketAddress) socket.getRemoteSocketAddress();
    return clientAddress.getAddress();
  }

  /**
   * Getting character set of client
   *
   * @param clientIp the ip of client
   * @return character set
   */
  public String getCharacterSet(String clientIp) {
    TCPConfiguration.Client configClient =
        tcpConfiguration.getClients().stream()
            .filter(c -> clientIp.equals(c.getName()))
            .findAny()
            .orElse(null);

    return configClient != null ? configClient.getCharSet() : StandardCharsets.UTF_8.name();
  }
}
