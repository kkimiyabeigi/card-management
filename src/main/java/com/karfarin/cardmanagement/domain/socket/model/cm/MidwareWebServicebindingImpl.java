/**
 * MidwareWebServicebindingImpl.java
 *
 * <p>This file was auto-generated from WSDL by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT)
 * WSDL2Java emitter.
 */
package com.karfarin.cardmanagement.domain.socket.model.cm;

public class MidwareWebServicebindingImpl implements MidwareWebService {
  public java.lang.String executeCommand(java.lang.String request) throws java.rmi.RemoteException {
    return null;
  }

  public long[] sendSms(
      java.lang.String appName,
      java.lang.String[] messageBodies,
      java.lang.String[] recipientNumbers,
      java.lang.String[] priorities)
      throws java.rmi.RemoteException {
    return null;
  }

  public int[] getRealMessageStatuses(long[] messagesId) throws java.rmi.RemoteException {
    return null;
  }
}
