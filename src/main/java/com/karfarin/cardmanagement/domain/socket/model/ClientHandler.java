package com.karfarin.cardmanagement.domain.socket.model;

import com.karfarin.cardmanagement.domain.socket.config.TCPConfiguration;
import com.karfarin.cardmanagement.domain.tosan.service.RequestDispatcherService;
import com.karfarin.cardmanagement.domain.tosan.service.RequestFactoryService;
import com.karfarin.cardmanagement.enums.CardSwitchType;
import com.karfarin.cardmanagement.util.ChannelManagementUtil;
import com.karfarin.cardmanagement.util.RouteingUtil;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.util.List;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/05 09:46 AM
 */
public class ClientHandler implements Runnable {
  private final Logger LOG = LoggerFactory.getLogger(getClass().getName());
  private final List<TCPConfiguration.Client> configClients;
  private final Client client;
  private final Socket clientSocket;

  /**
   * Constructor of class
   *
   * @param client the client
   * @param clientSocket the socket of client
   * @param configClients the clients of application.properties
   */
  public ClientHandler(
      Client client, Socket clientSocket, List<TCPConfiguration.Client> configClients) {
    this.client = client;
    this.clientSocket = clientSocket;
    this.configClients = configClients;
  }

  @Override
  public void run() {
    try {
      BufferedReader input =
          new BufferedReader(
              new InputStreamReader(clientSocket.getInputStream(), client.getCharset()));
      BufferedWriter output =
          new BufferedWriter(
              new OutputStreamWriter(clientSocket.getOutputStream(), client.getCharset()));

      boolean isSuccess = Boolean.TRUE;
      String receiveData;
      while (isSuccess) {
        try {
          receiveData = input.readLine();
          if (StringUtils.isNotEmpty(receiveData)) {
            String response = "";
            if (RouteingUtil.findRoute(receiveData).equals(CardSwitchType.ICS)) {
              LOG.info(
                  "The request was redirected to ICS with name: {}, IP: {}",
                  client.getName(),
                  client.getIp());

              TCPConfiguration.Client configClient =
                  configClients.stream()
                      .filter(c -> client.getIp().equals(c.getIp()))
                      .findAny()
                      .orElse(null);

              response =
                  configClient != null
                      ? ChannelManagementUtil.sendRequestForChannel(
                          configClient.getChannel().getIp(),
                          configClient.getChannel().getPort(),
                          client.getCharset(),
                          receiveData.concat(System.lineSeparator()))
                      : "";
              output.write(response);
              output.flush();

              LOG.info(
                  "Response of live channel for client with name: {}, IP: {} is: {}",
                  client.getName(),
                  client.getIp(),
                  response);
            } else if (RouteingUtil.findRoute(receiveData).equals(CardSwitchType.TOSAN)) {
              LOG.info(
                  "The request was redirected to TOSAN with name: {}, IP: {}",
                  client.getName(),
                  client.getIp());

              RequestFactoryService requestFactory = new RequestFactoryService();
              Object request = requestFactory.createChannelRequestObject(receiveData);
              RequestDispatcherService requestDispatcher = new RequestDispatcherService();
              response = requestDispatcher.sendRequest(request);

              LOG.info(
                  "Response of TOSAN for client with name: {}, IP: {} is: {}",
                  client.getName(),
                  client.getIp(),
                  response);
            }
            output.write(response);
            output.flush();
            isSuccess = Boolean.FALSE;
          }
        } catch (IOException e) {
          isSuccess = Boolean.FALSE;
          clientSocket.close();
          LOG.error("There is an error for client {} ", client, e);
        }
      }

      input.close();
      output.close();
      clientSocket.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
