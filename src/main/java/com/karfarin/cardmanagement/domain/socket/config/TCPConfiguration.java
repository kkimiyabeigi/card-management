package com.karfarin.cardmanagement.domain.socket.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/05 09:46 AM
 */
@ConfigurationProperties(prefix = "tcp")
public class TCPConfiguration {
  private List<Client> clients;
  private Integer port;
  private String client;

  public static class Client {
    private String name;
    private String ip;
    private String charSet;
    private Channel channel;

    public Client() {}

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getIp() {
      return ip;
    }

    public void setIp(String ip) {
      this.ip = ip;
    }

    public String getCharSet() {
      return charSet;
    }

    public void setCharSet(String charSet) {
      this.charSet = charSet;
    }

    public Channel getChannel() {
      return channel;
    }

    public void setChannel(Channel channel) {
      this.channel = channel;
    }
  }

  public static class Channel {
    private String ip;
    private Integer port;

    public Channel() {}

    public String getIp() {
      return ip;
    }

    public void setIp(String ip) {
      this.ip = ip;
    }

    public Integer getPort() {
      return port;
    }

    public void setPort(Integer port) {
      this.port = port;
    }
  }

  public Integer getPort() {
    return port;
  }

  public void setPort(Integer port) {
    this.port = port;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public List<Client> getClients() {
    return clients;
  }

  public void setClients(List<Client> clients) {
    this.clients = clients;
  }
}
