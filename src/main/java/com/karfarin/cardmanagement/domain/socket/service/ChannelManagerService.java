package com.karfarin.cardmanagement.domain.socket.service;

import com.karfarin.cardmanagement.domain.socket.model.cm.MidwareWebServicebindingStub;
import com.karfarin.cardmanagement.domain.socket.model.cm.MidwareWebServiceserviceLocator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.xml.rpc.ServiceException;
import java.rmi.RemoteException;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co
 * @date 2020/08/10 12:25 PM
 */
@Service
public class ChannelManagerService {
  private final Logger LOG = LoggerFactory.getLogger(getClass().getName());

  /**
   * Send request for channel manager
   *
   * @param request The request for channel manager
   * @return Response of channel manager
   * @throws RemoteException The exception from channel manager
   */
  public String executeCommand(String request) throws ServiceException, RemoteException {
    LOG.info("Send request for channel manager via request: {}", request);

    MidwareWebServicebindingStub binding =
        (MidwareWebServicebindingStub)
            new MidwareWebServiceserviceLocator().getMidwareWebServicePort();
    binding.setTimeout(60000);
    return binding.executeCommand(request);
  }
}
