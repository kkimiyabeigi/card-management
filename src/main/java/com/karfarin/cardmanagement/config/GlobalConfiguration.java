package com.karfarin.cardmanagement.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/05 09:46 AM
 */
@ConfigurationProperties(prefix = "global")
public class GlobalConfiguration {
  private String command;

  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command;
  }
}
