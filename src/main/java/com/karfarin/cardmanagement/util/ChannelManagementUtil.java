package com.karfarin.cardmanagement.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * @version 1.0.0
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @copyright Copyright &copy; FROM 2020 Negah Co
 * @date 2020/08/11 17:10 PM
 */
public class ChannelManagementUtil {

  /**
   * Sending request for live channel
   *
   * @param ip The ip of channel
   * @param port The port of channel
   * @param charset The charset of channel
   * @param message The message of request
   * @return the response of channel
   * @throws IOException The exception of socket
   */
  public static String sendRequestForChannel(String ip, int port, String charset, String message)
      throws IOException {
    Socket socket = new Socket(ip, port);
    socket.getOutputStream().write(message.getBytes(charset));
    socket.getOutputStream().flush();
    BufferedReader bufferedReader =
        new BufferedReader(new InputStreamReader(socket.getInputStream(), charset));
    String response = bufferedReader.readLine();
    socket.close();

    return response;
  }
}
