package com.karfarin.cardmanagement.util;

import com.karfarin.cardmanagement.domain.tosan.config.TosanConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Component
public class ApiUtil {

  private static TosanConfiguration tosanConfiguration;

  @Autowired
  public ApiUtil(TosanConfiguration tosanConfiguration) {
    ApiUtil.tosanConfiguration = tosanConfiguration;
  }

  /**
   * Sending rest api for tosan
   *
   * @param response The response of api
   * @param request The request of api
   * @param method The type of http method
   * @param uri The uri of url
   * @param <T> The generic of response
   * @param <G> The generic of request
   * @return The response od api
   */
  public static <T, G> T sendRestApi(T response, G request, HttpMethod method, String uri) {
    try {
      RestTemplate restTemplate = new RestTemplate();
      HttpHeaders headers = new HttpHeaders();
      headers.setContentType(MediaType.APPLICATION_XML);

      String url = tosanConfiguration.getUrl() + uri;
      HttpEntity<G> ownerCardRequest = new HttpEntity<>(request, headers);
      ResponseEntity<String> responseEntity =
          restTemplate.exchange(url, method, ownerCardRequest, String.class);

      return castStringToObject(responseEntity.getBody(), response);
    } catch (Exception e) {
      return null;
    }
  }

  /**
   * Converting xml string to Object
   *
   * @param xml The xml string
   * @param responseObject The object
   * @param <T> The generic object
   * @return Created object
   * @throws JAXBException
   */
  @SuppressWarnings("unchecked")
  public static <T> T castStringToObject(String xml, T responseObject) throws JAXBException {
    StringReader reader = new StringReader(xml);
    JAXBContext jaxbContext = JAXBContext.newInstance(responseObject.getClass());

    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    return (T) jaxbUnmarshaller.unmarshal(reader);
  }
}
