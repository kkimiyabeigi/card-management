package com.karfarin.cardmanagement.util;

import com.karfarin.cardmanagement.domain.socket.enums.ChannelParameterType;
import com.karfarin.cardmanagement.enums.CardSwitchType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @authors Kamyar Kimiyabeigi <k.kimiyabeigi@karafarinbank.ir>
 * @version 1.0.0
 * @copyright Copyright &copy; FROM 2020 Negah Co * @date 2020/08/15 11:30 AM
 */
@Component
public class RouteingUtil {
  public static Map<String, String> commands;

  /**
   * Finding correct route for request
   *
   * @param message The request
   * @return The type of card switch
   */
  public static CardSwitchType findRoute(String message) {
    String binTagName = getBinTagName(message);
    if (StringUtils.isEmpty(binTagName)) {
      return CardSwitchType.ALL;
    } else {
      String panValue = getValueByTagName(binTagName, message);
      String bin = StringUtils.isEmpty(panValue) ? null : panValue.substring(0, 6);
      if (StringUtils.isEmpty(bin)) return null;
      else if (bin.equalsIgnoreCase(CardSwitchType.ICS.getBin())) return CardSwitchType.ICS;
      else if (bin.equalsIgnoreCase(CardSwitchType.TOSAN.getBin())) return CardSwitchType.TOSAN;
    }
    return null;
  }

  /**
   * Getting bin tag name
   *
   * @param message The request
   * @return The bin tag name
   */
  private static String getBinTagName(String message) {
    String commandName = getValueByTagName(ChannelParameterType.COMMAND.name(), message);
    return commands.get(commandName);
  }

  /**
   * Getting value of element by tagName
   *
   * @param tagName The name of tag
   * @param message The message of channel
   * @return The value of element
   */
  public static String getValueByTagName(String tagName, String message) {
    String lowerCaseMessage = message.toLowerCase();
    String lowerCaseTagName = tagName.toLowerCase();
    String startTag = "<" + lowerCaseTagName + ">";
    String endTag = "</" + lowerCaseTagName + ">";
    int startIndex = lowerCaseMessage.indexOf(startTag);
    int endIndex = lowerCaseMessage.indexOf(endTag);
    return (startIndex == -1 || endIndex == -1)
        ? null
        : lowerCaseMessage.substring(startIndex + startTag.length(), endIndex).trim();
  }
}
