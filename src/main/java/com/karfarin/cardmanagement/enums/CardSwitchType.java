package com.karfarin.cardmanagement.enums;

public enum CardSwitchType {
  ALL(""),
  ICS("627488"),
  TOSAN("502910");

  private final String bin;

  CardSwitchType(String bin) {
    this.bin = bin;
  }

  public String getBin() {
    return bin;
  }
}
