package com.karfarin.cardmanagement;

import com.karfarin.cardmanagement.config.GlobalConfiguration;
import com.karfarin.cardmanagement.domain.socket.config.TCPConfiguration;
import com.karfarin.cardmanagement.domain.socket.service.ServerSocketService;
import com.karfarin.cardmanagement.domain.tosan.config.TosanConfiguration;
import com.karfarin.cardmanagement.util.RouteingUtil;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.Arrays;
import java.util.stream.Collectors;

@SpringBootApplication
@EnableConfigurationProperties({
  TCPConfiguration.class,
  TosanConfiguration.class,
  GlobalConfiguration.class
})
public class CardManagementApplication implements CommandLineRunner {

  private final ServerSocketService serverSocketService;
  private final GlobalConfiguration globalConfiguration;

  public CardManagementApplication(
      ServerSocketService serverSocketService, GlobalConfiguration globalConfiguration) {
    this.serverSocketService = serverSocketService;
    this.globalConfiguration = globalConfiguration;
  }

  public static void main(String[] args) {

    SpringApplication.run(CardManagementApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    RouteingUtil.commands =
        Arrays.stream(globalConfiguration.getCommand().split(","))
            .map(s -> s.split("-"))
            .collect(
                Collectors.toMap(
                    s -> s[0].toLowerCase(), s -> s.length > 1 ? s[1].toLowerCase() : ""));
    serverSocketService.startServerSocket();
  }
}
